# Transferring Data to / from Perlmutter Scratch

Perlmutter scratch is only accessible from Perlmutter login or compute
nodes.

NERSC has set up a dedicated [Globus Endpoint on
Perlmutter](./globus.md#availability) that has access to
Perlmutter Scratch as well as the Community and Homes File Systems at
NERSC. This is the recommended way to transfer large volumes of data
to/from Perlmutter scratch.

Alternatively, for small transfers you can use `scp` on a Perlmutter
login node.

Larger datasets could also be staged on the [Community File
System](../filesystems/community.md) (which is available on
Perlmutter) either with [Globus](./globus.md), or a `cp`,
or `rsync` on a [Data Transfer Node](../systems/dtn/index.md). Once
the data is on the Community File System, you can use `cp`, or `rsync`
from a Perlmutter login node to copy the data to Perlmutter
scratch.
