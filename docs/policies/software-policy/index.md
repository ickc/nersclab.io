# NERSC Software Support Policy

## Disallowed software

NERSC does not allow the use of:

- Classified or controlled military defense information
- Export controlled or [ITAR](https://www.pmddtc.state.gov/ddtc_public?id=ddtc_kb_article_page&sys_id=%2024d528fddbfc930044f9ff621f961987)
  codes or data
- Personally identifiable information
- Protected health information

Software meeting this definition is disallowed even if it is not explicitly listed 
as "Resticted".

Additionally, as stated in the "Security" tab of the
[ERCAP Request Form](https://www.nersc.gov/users/accounts/allocations/request-form/),
NERSC supports only open research intended to be published in open scientific
journals. Proprietary research is not allowed.
