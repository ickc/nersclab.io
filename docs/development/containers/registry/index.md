# Using NERSC's `registry.nersc.gov`

Users who wish to store images in the NERSC private registry are welcome
to request access to `registry.nersc.gov` via filing a ticket at `help.nersc.gov`.
Users who have completed Spin training may already have access. For any user who
does not have access, logging into `registry.nersc.gov` will fail with
"Invalid user name or password."

This registry is project-based, similar to our CFS and `/global/common/software`
filesystems. This means that each top-level project namespace can be shared
between all members of a project.

As a best practice, users should generally store images in their own namespace.
In the project “musicians”, we suggest that each user create and use their own
namespaces underneath, for example:

```shell
registry.nersc.gov/musicians
```

User `santana` should create their own namespace under their project `musicians`

```shell
registry.nersc.gov/musicians/santana
```

Individual projects may also agree upon other project-wide namespaces, for example:

```shell
registry.nersc.gov/musicians/smooth
registry.nersc.gov/musicians/santana
registry.nersc.gov/musicians/robthomas
```

`smooth`, a directory where users `santana` and `robthomas` can share their
collaboration materials, can be decided upon by users in the `musicians`
project.

Creating an image with this name/tag pattern and pushing it to `registry.nersc.gov`
will create the desired namespace under the `musicians` project. For example,
the user may build, tag, and push `registry.nersc.gov/musicians/smooth/radio:edit`. This
will create the `smooth` namespace on `registry.nersc.gov`.

If you have questions, please don’t hesitate to file a ticket at `help.nersc.gov`.
