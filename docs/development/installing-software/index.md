# Installing Software on NERSC File Systems

## Best Practice

When installing software on NERSC's file systems it is recommended to
use the `/global/common/software/<your NERSC project>`
directories. This filesystem is optimized for sharing software as it is
read-only on the compute nodes.

See our page about the [global file system](../../filesystems/global-common.md)
for more information.

Many user applications offer the option to specify the installation prefix when
building a software package; that should be set to your directory in 
`/global/common/software` as part of the configuration process.
