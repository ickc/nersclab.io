# Spack 

[Spack](https://spack.readthedocs.io) is a package manager designed to support
multiple versions and configurations of software on a wide variety of platforms
and environments. Spack is non-destructive: installing a new version of a
package does not break existing installations, so many configurations of the
same package can coexist.

## Using the NERSC-provided Spack

NERSC provides the Spack package manager via a modulefile. This Spack instance
is preconfigured by NERSC to integrate with the Perlmutter software environment.

In order to use the default Spack instance, run

```
module load spack
```

## Spack Commands

Shown below are some of the most common Spack commands you will use. For a complete list of commands, 
see the [Spack Command Reference](https://spack.readthedocs.io/en/latest/command_index.html).

| Command | Description                                        |
| ------- |----------------------------------------------------|
| `spack install <spec>` | Install a package by spec.                         |
| `spack find` | List installed packages.                           |
| `spack list` | List available packages.                           |
| `spack load <spec>` | Load a package into the current environment.       |
| `spack unload <spec>` | Unload a package from the current environment.     |
| `spack load --list` | List packages loaded into the current environment. |
| `spack env create <name>` | Create a new environment.                          |
| `spack env activate <name>` | Activate an environment.                           |
| `spack env deactivate` | Deactivate the current environment.                |
| `spack env status` | Show the status of the current environment.        |
| `spack env create -d <dir>` | Create an environment in a directory.              |
| `spack env activate -d <dir>` | Activate an environment in a directory.            |
| `spack concretize -f` | Force concretize an environment.                   |
| `spack spec <spec>` | Show what would be installed given a spec.         |
| `spack spec -Il <spec>` | Show the install status and dependencies with hashes. |
| `spack buildcache list` | List available binary packages.                    |
| `spack info <spec>` | Show information about a package.                  |
| `spack edit <spec>` | Open a package file in an editor.                  |
| `spack config get <section>` | Show the YAML configuration for a section.         |
| `spack config list` | List all configuration scopes.                     |
| `spack config blame <section>` | Show the YAML configuration file and line numbers for a section. |

In this coming section we will show you how to load and unload Spack packages into your user environment. 
There will be several examples which can be run on Perlmutter by loading the E4S module. 

## E4S Setup

In this coming section we will show you how to load and unload Spack packages into your user environment. 
There will be several examples which can be run on Perlmutter by loading E4S module. 

!!! note
    You can follow these examples by running the following commands to ensure you have a clean environment:

    ```
    module load e4s
    spack env activate gcc
    spack unload --all
    ```

## Loading Spack Packages 

!!! note

    Please see [E4S Setup](./spack.md#e4s-setup****) before running the examples below.

Unlike `module load` that is used for loading modules into your user environment, in Spack you must
use `spack load` to load a Spack package into your user environment. In this exercise we will show how to 
load PAPI into your user environment.

First, let's activate the `gcc` environment by running the following:

```
spack env activate gcc
```
    
Let's assume you want to use PAPI. First, you need to determine which Spack package you want to load.
We can run the following to show the installed versions of PAPI.

```
spack find papi
```

This will output a list of installed packages as shown:

```shell
==> Installed packages
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
papi@6.0.0.1
==> 1 installed package
```

Let's load PAPI into our user environment.
To specify the version use `@` and to specify the compiler use `%`. These symbols can also be combined
as is the case with loading GCC version 11.2.0.
Take note that Spack will load the dependencies
in your user environment which is the default behavior.

```shell
elvis@perlmutter> spack load papi%gcc@11.2.0
elvis@perlmutter> spack load --list
==> 13 loaded packages
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
berkeley-db@18.1.40  kbproto@1.0.7  libuv@1.44.1  ncurses@6.1   pkg-config@0.29.2  util-linux-uuid@2.36.2  xproto@7.0.31
inputproto@2.3.2     libmd@1.0.4    lzo@2.10      papi@6.0.0.1  rhash@1.4.2        xextproto@7.3.0
```

Your user environment will be updated. That includes `PATH`, `LD_LIBRARY_PATH`, and other
variables relevant for loading the package. Let's retrieve the papi version, we can do this using the `papi_version` program 
which will appear in your environment if you run `which papi` as shown below.

```shell
elvis@perlmutter> which papi_version
/global/common/software/spackecp/perlmutter/e4s-23.05/89639/spack/opt/spack/linux-sles15-zen3/gcc-11.2.0/papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt/bin/papi_version
```

Let's retrieve the PAPI version by running the program

```shell
elvis@perlmutter> papi_version
PAPI Version: 6.0.0.1
```

### View Shell Commands to Load the Package

!!! note

    Please see [E4S Setup](./spack.md#e4s-setup****) before running the examples below.

It can be informative to view the commands that Spack uses when a package is loaded. For example, to see
the shell commands used when `zlib` is loaded, use `spack load --sh zlib%gcc@11.2.0`.

```shell
elvis@perlmutter> spack load --sh zlib%gcc@11.2.0
export CMAKE_PREFIX_PATH=/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/hdf5-1.10.7-6opvtfcoaxox7oskb7dzisnb32fnk5ne:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q:/opt/cray/pe/mpich/8.1.22/ofi/gnu/9.1:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/cmake-3.23.1-p23fzuowp4yuitemelic7f65nwybthxd:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/rhash-1.4.2-xjxbsefahiue4isqlzwcobgc7sgfhmqm:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libuv-1.44.1-gitz4v6soymrlpymzvygqlqibktfsp7a:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libarchive-3.5.2-d4gpdx2rxj4calconu7cd6kgq2ev5roe:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zstd-1.5.2-rtaozj7xdfhxengzppbqbth4mya3edpq:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/xz-5.2.5-tyswlp4epqidsxgtgxyefciqoz2ju6oe:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/mbedtls-2.28.0-ucarxpp47pj7yo7abt5povg3swygev3q:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lzo-2.10-rlpwurcqberlfjlpk62go3arnneonv6u:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lz4-1.9.3-xxbkaphnofa6gxugxszqr5kzew7chrqa:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libiconv-1.16-3xwztgyaaco4thqqhacdst65mmnr5is3:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/expat-2.4.8-yvchkv23jw7po7meyuxckpre5uroipmd:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libbsd-0.11.5-uomjj7io3hhggbfyzofrw5jpw5chlv74:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libmd-1.0.4-iynqjjkz4jgtxagqy2o2tvspqfn3yvog:/global/common/software/nersc/pm-2021q4/easybuild/software/Nsight-Systems/2022.2.1:/global/common/software/nersc/pm-2021q4/easybuild/software/Nsight-Compute/2022.1.1;
export LD_LIBRARY_PATH=/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/hdf5-1.10.7-6opvtfcoaxox7oskb7dzisnb32fnk5ne/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q/lib:/opt/cray/pe/mpich/8.1.22/ofi/gnu/9.1/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/rhash-1.4.2-xjxbsefahiue4isqlzwcobgc7sgfhmqm/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libuv-1.44.1-gitz4v6soymrlpymzvygqlqibktfsp7a/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libarchive-3.5.2-d4gpdx2rxj4calconu7cd6kgq2ev5roe/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zstd-1.5.2-rtaozj7xdfhxengzppbqbth4mya3edpq/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/xz-5.2.5-tyswlp4epqidsxgtgxyefciqoz2ju6oe/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/mbedtls-2.28.0-ucarxpp47pj7yo7abt5povg3swygev3q/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lzo-2.10-rlpwurcqberlfjlpk62go3arnneonv6u/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lz4-1.9.3-xxbkaphnofa6gxugxszqr5kzew7chrqa/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libiconv-1.16-3xwztgyaaco4thqqhacdst65mmnr5is3/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/expat-2.4.8-yvchkv23jw7po7meyuxckpre5uroipmd/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libbsd-0.11.5-uomjj7io3hhggbfyzofrw5jpw5chlv74/lib:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libmd-1.0.4-iynqjjkz4jgtxagqy2o2tvspqfn3yvog/lib:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/math_libs/11.7/lib64:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7/extras/CUPTI/lib64:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7/extras/Debugger/lib64:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7/nvvm/lib64:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7/lib64:/global/common/software/nersc/pm-2022q3/sw/darshan/3.4.0/lib:/opt/cray/pe/papi/6.0.0.16/lib64:/opt/cray/pe/gcc/11.2.0/snos/lib64:/opt/cray/libfabric/1.15.2.0/lib64;
export MANPATH=/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/rhash-1.4.2-xjxbsefahiue4isqlzwcobgc7sgfhmqm/share/man:/usr/share/man:/usr/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libarchive-3.5.2-d4gpdx2rxj4calconu7cd6kgq2ev5roe/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/xz-5.2.5-tyswlp4epqidsxgtgxyefciqoz2ju6oe/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lz4-1.9.3-xxbkaphnofa6gxugxszqr5kzew7chrqa/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libiconv-1.16-3xwztgyaaco4thqqhacdst65mmnr5is3/share/man:/usr/share/man:/usr/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libbsd-0.11.5-uomjj7io3hhggbfyzofrw5jpw5chlv74/share/man:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libmd-1.0.4-iynqjjkz4jgtxagqy2o2tvspqfn3yvog/share/man:/usr/share/man:/usr/man:/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7/doc/man:/opt/cray/pe/perftools/22.09.0/man:/opt/cray/pe/papi/6.0.0.16/share/pdoc/man:/opt/cray/pe/gcc/11.2.0/snos/share/man:/opt/cray/pe/craype/2.7.19/man:/opt/cray/pe/mpich/8.1.22/ofi/man:/opt/cray/pe/mpich/8.1.22/man/mpich:/opt/cray/pe/libsci/22.11.1.2/man:/opt/cray/pe/man/csmlversion:/opt/cray/pe/dsmml/0.2.2/dsmml/man:/opt/cray/libfabric/1.15.2.0/share/man:/usr/share/lmod/lmod/share/man:/usr/local/man:/usr/share/man:/usr/man:;
export PKG_CONFIG_PATH=/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/hdf5-1.10.7-6opvtfcoaxox7oskb7dzisnb32fnk5ne/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q/lib/pkgconfig:/opt/cray/pe/mpich/8.1.22/ofi/gnu/9.1/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/rhash-1.4.2-xjxbsefahiue4isqlzwcobgc7sgfhmqm/lib/pkgconfig:/usr/share/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libuv-1.44.1-gitz4v6soymrlpymzvygqlqibktfsp7a/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libarchive-3.5.2-d4gpdx2rxj4calconu7cd6kgq2ev5roe/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zstd-1.5.2-rtaozj7xdfhxengzppbqbth4mya3edpq/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/zlib-1.2.12-ozmcyfjfv7i5gjjgklfsh43h67vzsuc5/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/xz-5.2.5-tyswlp4epqidsxgtgxyefciqoz2ju6oe/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lzo-2.10-rlpwurcqberlfjlpk62go3arnneonv6u/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/lz4-1.9.3-xxbkaphnofa6gxugxszqr5kzew7chrqa/lib/pkgconfig:/usr/share/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/expat-2.4.8-yvchkv23jw7po7meyuxckpre5uroipmd/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libbsd-0.11.5-uomjj7io3hhggbfyzofrw5jpw5chlv74/lib/pkgconfig:/global/common/software/spackecp/perlmutter/e4s-22.05/78535/spack/opt/spack/cray-sles15-zen3/gcc-11.2.0/libmd-1.0.4-iynqjjkz4jgtxagqy2o2tvspqfn3yvog/lib/pkgconfig:/usr/share/pkgconfig:/usr/lib64/pkgconfig:/usr/lib/pkgconfig:/opt/modulefiles/cudatoolkit:/global/common/software/nersc/pm-2022q3/sw/darshan/3.4.0/lib/pkgconfig:/opt/cray/pe/craype/2.7.19/pkg-config:/opt/cray/pe/dsmml/0.2.2/dsmml/lib/pkgconfig:/opt/cray/libfabric/1.15.2.0/lib64/pkgconfig;
export SPACK_LOADED_HASHES=ozmcyfjfv7i5gjjgklfsh43h67vzsuc5:6opvtfcoaxox7oskb7dzisnb32fnk5ne:nugfov2wsvpllczfy6ff7bytzt4oi34q:6yvclv72nfvhlx2fj72n5npigrgouojd:p23fzuowp4yuitemelic7f65nwybthxd:xjxbsefahiue4isqlzwcobgc7sgfhmqm:i6ri5efvxr243dakip6bk7n6b3qfruyb:gitz4v6soymrlpymzvygqlqibktfsp7a:d4gpdx2rxj4calconu7cd6kgq2ev5roe:rtaozj7xdfhxengzppbqbth4mya3edpq:ozmcyfjfv7i5gjjgklfsh43h67vzsuc5:tyswlp4epqidsxgtgxyefciqoz2ju6oe:ucarxpp47pj7yo7abt5povg3swygev3q:rlpwurcqberlfjlpk62go3arnneonv6u:xxbkaphnofa6gxugxszqr5kzew7chrqa:3xwztgyaaco4thqqhacdst65mmnr5is3:4g7s6qpmt2nsclyi2d2ndfek2ptey4m6:yvchkv23jw7po7meyuxckpre5uroipmd:uomjj7io3hhggbfyzofrw5jpw5chlv74:iynqjjkz4jgtxagqy2o2tvspqfn3yvog:zkrv7nh3jglxsimtfzld7vo4mhdqlh3u;
```

### Find the Install Prefix for a Package

For your application you may want to specify the location of the installed package as an environment
variable. For instance, let's set `PAPI_DIR` to the install prefix for the `papi` package. This can be 
done by running the `spack location -i` command which returns the install prefix for the package.

```shell
elvis@perlmutter> export PAPI_DIR=$(spack location -i papi%gcc@11.2.0)
```

## Unloading Spack Packages From Your Environment

!!! note

    Please see [E4S Setup](./spack.md#e4s-setup****) before running the examples below.

You can use `spack unload` to remove packages from your environment which functions similarly to `module unload`.
`spack unload` will only work on installed specs that were loaded in your environment. In the example below, we show
how to unload the `cmake` Spack package from the user environment. Take note as we unload cmake via `spack unload cmake` the
user environment was updated and it now resorts to the system cmake `/usr/bin/cmake`

```shell
elvis@perlmutter> spack load --list
==> 17 loaded packages
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
bzip2@1.0.6   curl@7.66.0  libarchive@3.6.2  libiconv@1.17  libuv@1.44.1  lzo@2.10        ncurses@6.1  xz@5.4.1     zstd@1.5.5
cmake@3.26.3  expat@2.5.0  libbsd@0.11.7     libmd@1.0.4    lz4@1.9.4     mbedtls@2.28.2  rhash@1.4.2  zlib@1.2.13

elvis@perlmutter> which cmake
/global/common/software/spackecp/perlmutter/e4s-23.05/89639/spack/opt/spack/linux-sles15-zen3/gcc-11.2.0/cmake-3.26.3-j6xprhqvjwkyngr534kqdxnp5uhwf6j4/bin/cmake

elvis@perlmutter> cmake --version
cmake version 3.26.3

CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

Now let's unload cmake and take note that your user environment is using the system installed cmake.

```shell
elvis@perlmutter> spack unload cmake
elvis@perlmutter> which cmake
/usr/bin/cmake

elvis@login40> cmake --version
cmake version 3.20.4

CMake suite maintained and supported by Kitware (kitware.com/cmake).

elvis@perlmutter> spack load --list
==> 16 loaded packages
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
bzip2@1.0.6  expat@2.5.0       libbsd@0.11.7  libmd@1.0.4   lz4@1.9.4  mbedtls@2.28.2  rhash@1.4.2  zlib@1.2.13
curl@7.66.0  libarchive@3.6.2  libiconv@1.17  libuv@1.44.1  lzo@2.10   ncurses@6.1     xz@5.4.1     zstd@1.5.5
```

If you want to remove all packages from your user environment you can run the following:

```shell
elvis@perlmutter> spack unload --all
elvis@perlmutter> spack load --list
==> 0 loaded packages
```

## Using Hashes for Packages with Multiple Versions

!!! note

    Please see [E4S Setup](./spack.md#e4s-setup****) before running the examples below.

Sometimes, you will need to use hashes instead of specs to load a specific package variant. Let's assume we want
to load HDF5 and we get the error, "`Use a more specific spec`". Note that in this example we have three specs
for same version but each has a different hash (The 7 character code proceeding the spec).

!!! error

    ```shell
    elvis@perlmutter> spack load hdf5
    ==> Error: hdf5 matches multiple packages.
      Matching packages:
        poditdt hdf5@1.14.1-2%gcc@=11.2.0 arch=linux-sles15-zen3
        74dfegs hdf5@1.14.1-2%gcc@=11.2.0 arch=linux-sles15-zen3
        2mtt4fm hdf5@1.14.1-2%gcc@=11.2.0 arch=linux-sles15-zen3
      Use a more specific spec (e.g., prepend '/' to the hash).
    ```

In this case, we can use the hash to uniquely identify the desired variant of HDF5. For example, if we want to
load the spec with hash *poditdt*, we would run the following:

```shell
elvis@perlmutter> spack load /poditdt
```

## Loading a Spack Package Without Dependencies

!!! note

    Please see [E4S Setup](./spack.md#e4s-setup****) before running the examples below.

The command `spack  load` will load a package and its dependencies which is **recommended** when using software, however
sometimes you don't want to load all the extra dependencies in your environment. Spack provides a way to
load only the package via `--only package` which will load the package and not its dependencies. In the example shown below,
we load one package. Take note of this compared to its normal behavior which loads all dependencies.

```shell
elvis@perlmutter> spack load --list
==> 0 loaded packages
elvis@perlmutter> spack load --only package /poditdt
elvis@perlmutter> spack load --list
==> 1 loaded package
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
hdf5@1.14.1-2

elvis@perlmutter> spack unload -a
elvis@perlmutter> spack load --list
==> 0 loaded packages
elvis@perlmutter> spack load /poditdt
elvis@perlmutter> spack load --list
==> 21 loaded packages
-- linux-sles15-zen3 / gcc@11.2.0 -------------------------------
bzip2@1.0.6   cray-mpich@os  expat@2.5.0  hdf5@1.14.1-2     libbsd@0.11.7  libmd@1.0.4   lz4@1.9.4  mbedtls@2.28.2  pkg-config@0.29.2  xz@5.4.1     zstd@1.5.5
cmake@3.26.3  curl@7.66.0    gmake@4.4.1  libarchive@3.6.2  libiconv@1.17  libuv@1.44.1  lzo@2.10   ncurses@6.1     rhash@1.4.2        zlib@1.2.13
```

## Recommended setup using environments

The `e4s/<version>` modules comes with several environments such as: `gcc`, `nvhpc`, `cce`, `cuda`. 
The `cuda` environment provides packages built with CUDA support with the `gcc@11.2.0` compiler. Depending on the 
environment you are using, please consider loading the appropriate modules

1. Load the `PrgEnv-gnu`, `cpu` module when using `gcc` environment
2. Load the `PrgEnv-cce`, `cpu` module when using `cce` environment
3. Load the `PrgEnv-nvhpc`, `cpu` module when using `nvhpc` environment
4. Load the `PrgEnv-gnu`, `gpu` module when using `cuda` environment

When in doubt, please refer to the compiler settings to determine the list of modules used. To retrieve a list of compilers you can
do `spack compiler list`. To query a compiler instance like `gcc@11.2.0` you can run `spack compiler info gcc@11.2.0`.

In the `cuda` environment, you will see packages are built with variants `+cuda cuda_arch=80` that are optimized for 
NVIDIA A100 GPUs (`+cuda cuda_arch=80`). You should make sure the `craype-accel-nvidia` modulefile is loaded. Usually this is loaded
with the `gpu` modulefile at login. You can check the accelerator target by running the following:

```shell
elvis@perlmutter> echo $CRAY_ACCEL_TARGET
nvidia80
```

## Developer Workflow

Please see example below illustrating how to install a spack packages in your user environment. 

??? "Example building software and generating modules via spack"
    
    Shown below is an example script used to install `papi` into a spack environment. You can copy this script and
    the corresponding spack.yaml below to your home directory and run it. Make sure both scripts are present and in
    same directory.

    ```bash
    #!/bin/bash
    rm -rf $HOME/spack-workspace/perlmutter/modules
    rm -rf $HOME/spack-workspace/perlmutter/software
    module load e4s/23.05
    source $SPACK_ROOT/bin/spack-setup.sh
    spack env status
    spack spec zlib
    rm -rf $HOME/e4s-demo
    spack env create -d $HOME/e4s-demo spack.yaml
    spack env activate -d $HOME/e4s-demo
    ls -l $HOME/e4s-demo
    spack config list
    echo  $SPACK_SYSTEM_CONFIG_PATH
    ls -l $SPACK_SYSTEM_CONFIG_PATH
    cat $SPACK_ROOT/etc/spack/config.yaml
    spack clean -m
    spack install
    spack location -i papi
    cat $SPACK_ROOT/etc/spack/modules.yaml
    spack module tcl refresh --delete-tree -y
    spack module tcl find --full-path papi
    module use $HOME/spack-workspace/perlmutter/modules/$(spack arch)
    ml -t av papi
    ```

    Shown below is the spack configuration for installing papi

    ```yaml
    # This is a Spack Environment file.
    #
    # It describes a set of packages to be installed, along with
    # configuration settings.
    spack:
      # add package specs to the `specs` list
      specs:
      - papi
      view: true
    ```

    Shown below is the output of the script, we named this file `workflow.sh` 

    ```console
    elvis@login40> bash workflow.sh
    _______________________________________________________________________________________________________
     The Extreme-Scale Scientific Software Stack (E4S) is accessible via the Spack package manager.

     In order to access the production stack, you will need to load a spack environment. Here are some tips to get started:


     'spack env list' - List all Spack environments
     'spack env activate gcc' - Activate the "gcc" Spack environment
     'spack env status' - Display the active Spack environment
     'spack load amrex' - Load the "amrex" Spack package into your user environment

     For additional support, please refer to the following references:

       NERSC E4S Documentation: https://docs.nersc.gov/applications/e4s/
       E4S Documentation: https://e4s.readthedocs.io
       Spack Documentation: https://spack.readthedocs.io/en/latest/
       Spack Slack: https://spackpm.slack.com

    ______________________________________________________________________________________________________

    /opt/cray/pe/lmod/lmod/init/bash: line 197: unalias: spacktivate: not found
    Creating python virtual environment in /global/homes/e/elvis/.spack-pyenv
    WARNING: You are using pip version 20.0.2; however, version 21.3.1 is available.
    You should consider upgrading via the '/global/homes/e/elvis/.spack-pyenv/bin/python3 -m pip install --upgrade pip' command.
    Package    Version
    ---------- -------
    cffi       1.15.1
    clingo     5.6.2
    pip        20.0.2
    pycparser  2.21
    setuptools 44.1.1
    WARNING: You are using pip version 20.0.2; however, version 21.3.1 is available.
    You should consider upgrading via the '/global/homes/e/elvis/.spack-pyenv/bin/python3 -m pip install --upgrade pip' command.
    Your python interpreter used by spack is /global/homes/e/elvis/.spack-pyenv/bin/python
    ==> No active environment
    Input spec
    --------------------------------
    zlib
    
    Concretized
    --------------------------------
    zlib@1.2.13%gcc@11.2.0+optimize+pic+shared build_system=makefile arch=linux-sles15-zen3
    
    ==> Created environment in /global/homes/e/elvis/e4s-demo
    ==> You can activate this environment with:
    ==>   spack env activate /global/homes/e/elvis/e4s-demo
    total 1
    -rw-rw-r-- 1 elvis elvis 205 Sep  8 12:44 spack.yaml
    compilers concretizer mirrors repos packages modules config upstreams bootstrap ci cdash
    /global/common/software/spackecp/perlmutter/spack_settings
    total 10
    -rw-rw-r-- 1 e4s spackecp 3108 Aug 11 12:31 compilers.yaml
    -rw-rw-r-- 1 e4s spackecp 4931 Aug 11 12:31 packages.yaml
    config:
      build_stage: $HOME/spack-workspace/perlmutter/build_stage
      misc_cache: $HOME/spack-workspace/perlmutter/misc_cache
      install_tree:
        root: $HOME/spack-workspace/perlmutter/software
      source_cache: $HOME/.spack/cache
    
    ==> Removing cached information on repositories
    ==> Concretized papi
     -   frkekrt  papi@6.0.0.1%gcc@11.2.0~cuda+example~infiniband~lmsensors~nvml~powercap~rapl~rocm~rocm_smi~sde+shared~static_tools build_system=autotools arch=linux-sles15-zen3
    
    ==> Installing papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt
    ==> Fetching file:///global/common/software/spackecp/mirrors/perlmutter-e4s-23.05/build_cache/linux-sles15-zen3-gcc-11.2.0-papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt.spec.json.sig
    gpg: Signature made Mon 31 Jul 2023 03:05:53 PM PDT
    gpg:                using RSA key B5FDE18F615783AF078ED29C3BD6B0E9935AEB8F
    gpg: Good signature from "GPG Key - e4s <shahzebsiddiqui@lbl.gov>" [ultimate]
    ==> Fetching file:///global/common/software/spackecp/mirrors/perlmutter-e4s-23.05/build_cache/linux-sles15-zen3/gcc-11.2.0/papi-6.0.0.1/linux-sles15-zen3-gcc-11.2.0-papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt.spack
    ==> Extracting papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt from binary cache
    ==> papi: Successfully installed papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt
      Search: 0.00s.  Fetch: 1.02s.  Install: 1.33s.  Total: 2.36s
    [+] /global/homes/s/elvis/spack-workspace/perlmutter/software/linux-sles15-zen3/gcc-11.2.0/papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt
    ==> Updating view at /global/homes/e/elvis/e4s-demo/.spack-env/view
    ==> Warning: detected custom TCL modules configuration in /global/common/software/spackecp/perlmutter/e4s-23.05/89639/spack/etc/spack/modules.yaml, while TCL module file generation for the default module set is disabled. In Spack v0.20 module file generation has been disabled by default. To enable it run:
    
        $ spack config add 'modules:default:enable:[tcl]'
    
    /global/homes/e/elvis/spack-workspace/perlmutter/software/linux-sles15-zen3/gcc-11.2.0/papi-6.0.0.1-frkekrtdhzrwqsr2nvuntrddpju7etpt
    modules:
      default:
        roots:
          tcl: $HOME/spack-workspace/perlmutter/modules
        tcl:
          naming_scheme: '{name}/{version}-{compiler.name}-{compiler.version}'
          all:
            conflict:
            - '{name}'
            environment:
              set:
                '{name}_ROOT': '{prefix}'
    ==> Regenerating tcl module files
    /global/homes/e/elvis/spack-workspace/perlmutter/modules/linux-sles15-zen3/papi/6.0.0.1-gcc-11.2.0-frkekrt
    /global/homes/e/elvis/spack-workspace/perlmutter/modules/linux-sles15-zen3:
    papi/6.0.0.1-gcc-11.2.0-frkekrt
    /opt/cray/pe/lmod/modulefiles/core:
    papi/6.0.0.16
    papi/7.0.0.1
    ```

### Bootstrapping Spack  

There is a known issue with Spack unable to bootstrap clingo on Perlmutter see <https://github.com/spack/spack/issues/28315>.
The main issue is getting `clingo` installed which must be in your python environment. In order to get around this we have
provided a script you can source which will create a python environment in your $HOME directory and install clingo. You
can source the following script based on your shell type.

!!! note
    If you already have clingo in your python environment you don't need to source this script.

```shell
# bash, sh, zsh users
source $SPACK_ROOT/bin/spack-setup.sh

# csh or tcsh users
source $SPACK_ROOT/bin/spack-setup.csh
```

We recommend you run the following to see if Spack will concretize a spec and not try to bootstrap clingo. If
you are in a Spack environment, please deactivate the environment using `despacktivate` command.
You can confirm your active environment by running `spack env status`. Next try running `spack spec zlib` 
to check if Spack will concretize `zlib` which will use `clingo` installed in your Python environment.

```shell
(.spack-pyenv) elvis@perlmutter> despacktivate
(.spack-pyenv) elvis@perlmutter> spack env status
==> No active environment

(.spack-pyenv) elvis@perlmutter> spack spec zlib
Input spec
--------------------------------
zlib

Concretized
--------------------------------
zlib@1.2.12%gcc@11.2.0+optimize+pic+shared patches=0d38234 arch=cray-sles15-zen3
```

We recommend you create an environment in your user space to install your Spack stack. This
can be done using the `spack env create -d <dir>` command. This will create a directory with an empty `spack.yaml`. 

In example below, we will create a environment in our home directory called `e4s-demo` and activate the spack environment.

```shell
(.spack-pyenv) elvis@perlmutter> spack env create -d $HOME/e4s-demo
==> Created environment in /global/homes/s/elvis/e4s-demo
==> You can activate this environment with:
==>   spack env activate /global/homes/s/elvis/e4s-demo

(.spack-pyenv) elvis@perlmutter> spack env activate $HOME/e4s-demo

(.spack-pyenv) elvis@perlmutter> ls -l $HOME/e4s-demo
total 1
-rw-rw---- 1 elvis elvis 199 Jul 19 12:10 spack.yaml
```

### Querying Spack Configuration

The `spack config` command can be used to query [Spack configuration](https://spack.readthedocs.io/en/latest/configuration.html).
Spack has several [configuration scopes](https://spack.readthedocs.io/en/latest/configuration.html#configuration-scopes)
where configuration can be pulled. The configuration scope that is most relevant to you are `site`, `user` and `defaults`.

You can use `spack config get [section]` to query the configuration section. If you are unsure of all the configuration sections,
you can run 

```shell
spack config list 
```

If you want to query the package configuration, you can run `spack config get packages`. The `spack config --scope` option
can be useful to query by configuration scope. We have set environment `$SPACK_SYSTEM_CONFIG_PATH` in our e4s modules that 
provides our global spack settings for all e4s stacks. The environment should be set to the following which should contain the
following configuration files:

```shell
elvis@perlmutter> echo  $SPACK_SYSTEM_CONFIG_PATH
/global/common/software/spackecp/perlmutter/spack_settings

elvis@perlmutter> ls -l $SPACK_SYSTEM_CONFIG_PATH
total 10
-rw-rw-r-- 1 e4s spackecp 3609 Mar  7 07:27 compilers.yaml
-rw-rw-r-- 1 e4s spackecp 4494 Mar  7 07:27 packages.yaml
```

This Spack instance is configured to install packages in your `HOME` directory, the Spack site configuration
can be found in this directory `$SPACK_ROOT/etc/spack/` which applies configuration
to all users but this can be overridden in your `spack.yaml`. Take note of the following settings below for build stage,
and installation tree. 

```shell
(.spack-pyenv) elvis@perlmutter> cat $SPACK_ROOT/etc/spack/config.yaml
config:
  build_stage: $HOME/spack-workspace/perlmutter/build_stage
  misc_cache: $HOME/spack-workspace/perlmutter/misc_cache
  install_tree:
    root: $HOME/spack-workspace/perlmutter/software
  source_cache: $HOME/.spack/cache
```

**If you plan on installing a large software stack, we recommend you use $CFS since it will have more storage space.** 

### Installing packages

In the example above, we had the following spack configuration that was responsible for installing `papi` as shown below.

```yaml
# This is a Spack Environment file.
#
# It describes a set of packages to be installed, along with
# configuration settings.
spack:
  # add package specs to the `specs` list
  specs:
  - papi
  view: true
```

If you want to add more packages in your environment, just add them under the `specs` keyword. The spack configuration 
file (spack.yaml) is meant to be customized to your preference. You can override any global configurations in your `spack.yaml`.

### Creating Modulefiles via spack

In this section, we will explain the module generation step via spack. We recommend you review the 
[modules](https://spack.readthedocs.io/en/latest/module_file_support.html) documentation 
in spack. The spack configuration for modules is defined in `modules.yaml`. 

Shown below is the site configuration for `modules.yaml`, we have configured the
module root to be `$HOME/spack-workspace/perlmutter/modules` where TCL modules will be generated.
Furthermore, we have customized the TCL module setting when generating modules.
You are welcome to override these configurations in your `spack.yaml`.

```shell
(.spack-pyenv) elvis@perlmutter> cat $SPACK_ROOT/etc/spack/modules.yaml
modules:
  default:
    roots:
      tcl: $HOME/spack-workspace/perlmutter/modules
    tcl:
      naming_scheme: '{name}/{version}-{compiler.name}-{compiler.version}'
      all:
        conflict:
        - '{name}'
        environment:
          set:
            '{name}_ROOT': '{prefix}'
```

We can generate the modules using `spack module tcl refresh`, this will generate the modules in the directory. The `--delete-tree`
will delete the existing modules in the directory. The `-y` option will automatically answer yes to all questions. 

```shell
(.spack-pyenv) elvis@perlmutter> spack module tcl refresh --delete-tree -y
==> Regenerating tcl module files
(.spack-pyenv) elvis@perlmutter> spack module tcl find --full-path papi
/global/homes/e/elvis/spack-workspace/perlmutter/modules/linux-sles15-zen3/papi/6.0.0.1-gcc-11.2.0-frkekrt
```

!!! note
    **You should not copy or update modulefiles in this directory since they will be overwritten when you run `spack module tcl refresh`**.

If you want to add view the newly created modules, just add the directory path to MODULEPATH by running the following:

```shell
(.spack-pyenv) elvis@perlmutter> module use $HOME/spack-workspace/perlmutter/modules/$(spack arch)
```

Now we see the modulefile `papi/6.0.0.1-gcc-11.2.0-frkekrt` is visible and can be loaded as any other software package.

```shell
(.spack-pyenv) elvis@perlmutter> ml -t av papi
/global/homes/e/elvis/spack-workspace/perlmutter/modules/linux-sles15-zen3:
papi/6.0.0.1-gcc-11.2.0-frkekrt
/opt/cray/pe/lmod/modulefiles/core:
papi/6.0.0.16
papi/7.0.0.1
```

## Binary Cache Mirror

We have setup a [binary cache](https://spack.readthedocs.io/en/latest/binary_caches.html) mirror named 
**perlmutter-e4s-23.05** that can be added into your `spack.yaml`. 

You can check this by running `spack mirror list` to see the list of mirrors.

```shell
elvis@perlmutter>  spack mirror list
perlmutter-e4s-23.05    file:///global/common/software/spackecp/mirrors/perlmutter-e4s-23.05
source_mirror           file:///global/cfs/cdirs/m3503/mirrors/source_mirror
spack-public            https://mirror.spack.io
```

Spack has support signing and verifying packages via 
[GPG Key](https://spack.readthedocs.io/en/latest/getting_started.html#gpg-signing) which is required 
when installing packages from the binary cache. To learn more about the binary cache, please see 
https://spack-tutorial.readthedocs.io/en/latest/tutorial_binary_cache.html#setting-up-a-binary-cache-mirror

First, you will need to install the GPG key locally, **note this is only required once**. Please run the following command.

```shell
elvis@perlmutter> spack buildcache keys -it
==> Fetching file:///global/common/software/spackecp/mirrors/perlmutter-e4s-22.05/build_cache/_pgp/B5FDE18F615783AF078ED29C3BD6B0E9935AEB8F.pub
gpg: key 3BD6B0E9935AEB8F: public key "GPG Key - e4s <shahzebsiddiqui@lbl.gov>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: inserting ownertrust of 6
```

You can see list of gpg keys by running `gpg -k` make sure you see the following entry in the output.

```shell
pub   rsa2048 2022-02-23 [SC]
      B5FDE18F615783AF078ED29C3BD6B0E9935AEB8F
uid           [ultimate] GPG Key - e4s <shahzebsiddiqui@lbl.gov>
sub   rsa2048 2022-02-23 [E]
```

If you have successfully imported the gpg key you should be able to run `spack buildcache list` to see all the specs 
in the binary cache.

We have set the environment variable `SPACK_GNUPGHOME` which points to `$HOME/.gnupg` in case you run any commands like 
`spack gpg` which will typically fail since Spack attempts to write GPG keys into the Spack instance at 
`$SPACK_ROOT/spack/opt/gpg` which you won't have access to.

If you are still in your environment that you created in section [Developer Workflow](./spack.md#developer-workflow), 
let's install a package from the binary cache, you will notice that Spack will install packages from the binary cache instead
of building from source.

```shell
elvis@perlmutter> spack install raja@0.14.0~cuda+examples+exercises~ipo+openmp~rocm+shared~tests build_type=RelWithDebInfo
==> Installing pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q
==> Fetching file:///global/common/software/spackecp/mirrors/perlmutter-e4s-22.05/build_cache/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0/cray-sles15-zen3-gcc-11.2.0-pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q.spack
==> Extracting pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q from binary cache
gpg: Signature made Fri 14 Oct 2022 08:12:17 AM PDT
gpg:                using RSA key B5FDE18F615783AF078ED29C3BD6B0E9935AEB8F
gpg: Good signature from "GPG Key - e4s <shahzebsiddiqui@lbl.gov>" [ultimate]
[+] /global/homes/s/elvis/spack-workspace/perlmutter/software/cray-sles15-zen3/gcc-11.2.0/pkgconf-1.8.0-nugfov2wsvpllczfy6ff7bytzt4oi34q
==> Installing berkeley-db-18.1.40-hafeanvncfcwgstenlf5cc7ttyobmgt3
==> Fetching file:///global/common/software/spackecp/mirrors/perlmutter-e4s-22.05/build_cache/cray-sles15-zen3/gcc-11.2.0/berkeley-db-18.1.40/cray-sles15-zen3-gcc-11.2.0-berkeley-db-18.1.40-hafeanvncfcwgstenlf5cc7ttyobmgt3.spack
==> Extracting berkeley-db-18.1.40-hafeanvncfcwgstenlf5cc7ttyobmgt3 from binary cache
gpg: Signature made Thu 13 Oct 2022 11:52:15 AM PDT
gpg:                using RSA key B5FDE18F615783AF078ED29C3BD6B0E9935AEB8F
gpg: Good signature from "GPG Key - e4s <shahzebsiddiqui@lbl.gov>" [ultimate]
[+] /global/homes/s/elvis/spack-workspace/perlmutter/software/cray-sles15-zen3/gcc-11.2.0/berkeley-db-18.1.40-hafeanvncfcwgstenlf5cc7ttyobmgt3

...
```

## Containerized Deployment with Shifter

You can access the E4S container using Shifter on Perlmutter.

To launch an E4S 21.11 container on a single node with 4 GPUs you can do the following:

```shell
$> salloc --nodes 1 --qos interactive --time 01:00:00 --constraint gpu --gpus 4 --account=<account>_g

$> shifterimg images | grep ecpe4s
perlmutter docker     READY    e2da787b6a   2021-11-15T07:58:10 ecpe4s/ubuntu20.04-gpu-x86_64:21.11

$> shifter -E --image=ecpe4s/ubuntu20.04-gpu-x86_64:21.11 /bin/bash --rcfile /etc/bashrc
(base) $ which spack
/spack/bin/spack
```

Once you are in the container you can access the software stack by running `spack find`.

??? "Output of `spack find` inside the container"

    ```shell
    adiak@0.2.1%gcc@9.3.0: ~ipo+mpi+shared build_type=RelWithDebInfo
    adios@1.13.1%gcc@9.3.0: +blosc~bzip2~fortran~hdf5~infiniband+lz4+mpi~netcdf+shared+sz~szip+zfp+zlib patches=01113e9efb929d71c28bf33cc8b7f215d85195ec700e99cb41164e2f8f830640,8ae17f655248e87cbab1d1ed794e15364a38d2f5f8d971b1086702f72d79bd42,d24b79b795f66e40ddcd331ea4be896ac9c393d6f68f4318616d23928b0694e9 staging=none
    adios2@2.7.1%gcc@9.3.0: +blosc+bzip2~dataman~dataspaces~endian_reverse+fortran~hdf5~ipo+mpi+pic+png~python+shared+ssc+sst+sz+zfp build_type=Release patches=8d301e8232baf4049b547f22bd73774309662017a62dac36360d2965907062bf
    adlbx@1.0.0%gcc@9.3.0:
    adol-c@2.7.2%gcc@9.3.0: ~advanced_branching+atrig_erf~boost+doc+examples~openmp~sparse
    alquimia@1.0.9%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    aml@0.1.0%gcc@9.3.0:
    amrex@21.11%gcc@9.3.0: ~amrdata~cuda~eb~fortran~hdf5~hypre~ipo+linear_solvers+mpi~openmp~particles~petsc~pic~plotfile_tools+rocm~shared~sundials amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none dimensions=3 precision=double
    amrex@21.11%gcc@9.3.0: ~amrdata~cuda~eb~fortran~hdf5~hypre~ipo+linear_solvers+mpi~openmp~particles~petsc~pic~plotfile_tools~rocm~shared~sundials amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none dimensions=3 precision=double
    amrex@21.11%gcc@9.3.0: ~amrdata+cuda~eb~fortran~hdf5~hypre~ipo+linear_solvers+mpi~openmp~particles~petsc~pic~plotfile_tools~rocm~shared~sundials amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70 dimensions=3 precision=double
    amrex@21.11%gcc@9.3.0: ~amrdata+cuda~eb~fortran~hdf5~hypre~ipo+linear_solvers+mpi~openmp~particles~petsc~pic~plotfile_tools~rocm~shared~sundials amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80 dimensions=3 precision=double
    ant@1.10.7%gcc@9.3.0:
    arborx@1.1%gcc@9.3.0: ~cuda~ipo+mpi~openmp~rocm+serial~sycl~trilinos build_type=RelWithDebInfo
    argobots@1.1%gcc@9.3.0: ~affinity~debug+perf~stackunwind~tool~valgrind stackguard=none
    arpack-ng@3.8.0%gcc@9.3.0: +mpi+shared
    ascent@0.7.1%gcc@9.3.0: ~adios~adios2~babelflow~cuda~doc~dray~fides+fortran~ipo~mfem+mpi+openmp~python+serial+shared+test+vtkh build_type=RelWithDebInfo cuda_arch=none
    asio@1.20.0%gcc@9.3.0: ~boost_coroutine~boost_regex~separate_compilation cxxstd=17
    assimp@5.0.1%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    autoconf@2.69%gcc@9.3.0:  patches=35c449281546376449766f92d49fc121ca50e330e60fefcfc9be2af3253082c2,7793209b33013dc0f81208718c68440c5aae80e7a1c4b8d336e382525af791a7,a49dd5bac3b62daa0ff688ab4d508d71dbd2f4f8d7e2a02321926346161bf3ee
    autoconf-archive@2019.01.06%gcc@9.3.0:
    automake@1.16.3%gcc@9.3.0:
    axl@0.3.0%gcc@9.3.0: +bbapi~bbapi_fallback~dw~ipo+shared async_api=daemon build_type=RelWithDebInfo
    axl@0.5.0%gcc@9.3.0: +bbapi~bbapi_fallback~dw~ipo+shared async_api=daemon build_type=RelWithDebInfo
    axom@0.5.0%gcc@9.3.0: +cpp14~cuda~debug~devtools+examples+fortran+hdf5~ipo+lua~mfem+mpi+openmp~python+raja~scr+shared+tools+umpire build_type=RelWithDebInfo cuda_arch=none
    berkeley-db@18.1.40%gcc@9.3.0: +cxx~docs+stl patches=b231fcc4d5cff05e5c3a4814f6a5af0e9a966428dc2176540d2c05aff41de522
    berkeley-db@18.1.40%gcc@11.1.0: +cxx~docs+stl patches=b231fcc4d5cff05e5c3a4814f6a5af0e9a966428dc2176540d2c05aff41de522
    binutils@2.33.1%gcc@9.3.0: ~gas+gold+headers~interwork+ld+libiberty~lto~nls+plugins libs=shared,static
    binutils@2.36.1%gcc@9.3.0: ~gas+gold+headers~interwork+ld+libiberty~lto~nls+plugins libs=shared,static patches=a51b7bf4607645235be01683a3e2deb3a87d767cc124ea4ef6febf9f32abf4a7
    binutils@2.36.1%gcc@11.1.0: ~gas+gold+headers~interwork+ld+libiberty~lto~nls+plugins libs=shared,static patches=a51b7bf4607645235be01683a3e2deb3a87d767cc124ea4ef6febf9f32abf4a7
    bison@3.8.2%gcc@9.3.0:
    blaspp@2021.04.01%gcc@9.3.0: ~cuda~ipo+openmp+rocm+shared amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none
    blaspp@2021.04.01%gcc@9.3.0: ~cuda~ipo+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    blaspp@2021.04.01%gcc@9.3.0: +cuda~ipo+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    blt@0.4.1%gcc@9.3.0:
    bmi@main%gcc@9.3.0:
    bolt@2.0%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    boost@1.77.0%gcc@9.3.0: +atomic+chrono~clanglibcpp~container~context~coroutine+date_time~debug+exception~fiber+filesystem+graph~icu+iostreams+locale+log+math~mpi+multithreaded~numpy~pic+program_options~python+random+regex+serialization+shared+signals~singlethreaded+system~taggedlayout+test+thread+timer~versionedlayout+wave cxxstd=17 patches=93f4aad8f88d1437e50d95a2d066390ef3753b99ef5de24f7a46bc083bd6df06 visibility=global
    boost@1.77.0%gcc@9.3.0: +atomic+chrono~clanglibcpp~container~context~coroutine+date_time~debug+exception~fiber+filesystem+graph~icu+iostreams+locale+log+math~mpi+multithreaded~numpy~pic+program_options~python+random+regex+serialization+shared+signals~singlethreaded+system~taggedlayout+test+thread+timer~versionedlayout+wave cxxstd=98 patches=93f4aad8f88d1437e50d95a2d066390ef3753b99ef5de24f7a46bc083bd6df06 visibility=global
    butterflypack@2.0.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    bzip2@1.0.8%gcc@9.3.0: ~debug~pic+shared
    bzip2@1.0.8%gcc@11.1.0: ~debug~pic+shared
    c-blosc@1.21.0%gcc@9.3.0: +avx2~ipo build_type=RelWithDebInfo
    cabana@0.4.0%gcc@9.3.0: ~arborx~cuda~heffte~hypre~ipo+mpi~openmp~pthread~rocm+serial+shared~sycl build_type=RelWithDebInfo
    caliper@2.7.0%gcc@9.3.0: +adiak~cuda~fortran+gotcha~ipo+libdw~libpfm+libunwind+mpi+papi+sampler+shared~sosflow build_type=RelWithDebInfo cuda_arch=none
    caliper@2.7.0%gcc@9.3.0: +adiak+cuda~fortran+gotcha~ipo+libdw~libpfm+libunwind+mpi+papi+sampler+shared~sosflow build_type=RelWithDebInfo cuda_arch=70
    caliper@2.7.0%gcc@9.3.0: +adiak+cuda~fortran+gotcha~ipo+libdw~libpfm+libunwind+mpi+papi+sampler+shared~sosflow build_type=RelWithDebInfo cuda_arch=80
    camp@0.1.0%gcc@9.3.0: ~cuda~ipo~rocm~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    camp@0.2.2%gcc@9.3.0: ~cuda~ipo~rocm~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    camp@0.2.2%gcc@9.3.0: +cuda~ipo~rocm~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    camp@0.2.2%gcc@9.3.0: +cuda~ipo~rocm~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    catalyst@5.6.0%gcc@9.3.0: ~essentials~extras~ipo+osmesa~python~python3+rendering build_type=RelWithDebInfo
    chai@2.4.0%gcc@9.3.0: ~benchmarks~cuda~enable_pick+examples~ipo~openmp~raja~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    chai@2.4.0%gcc@9.3.0: ~benchmarks+cuda~enable_pick+examples~ipo~openmp~raja~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    chai@2.4.0%gcc@9.3.0: ~benchmarks+cuda~enable_pick+examples~ipo~openmp~raja~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    charliecloud@0.25%gcc@9.3.0: ~docs
    cinch@master%gcc@9.3.0:
    cmake@3.21.4%gcc@9.3.0: ~doc+ncurses+openssl+ownlibs~qt build_type=Release
    cmake@3.21.4%gcc@11.1.0: ~doc+ncurses+openssl+ownlibs~qt build_type=Release
    conduit@0.7.2%gcc@9.3.0: ~adios~doc~doxygen+fortran+hdf5+hdf5_compat~ipo+mpi~parmetis~python+shared~silo+test~zfp build_type=RelWithDebInfo patches=89d1829ad52f503f6179e43efddf998c239a95c14ca1f248463a3f61ad7d5cf7
    conduit@0.7.2%gcc@9.3.0: ~adios~doc~doxygen+fortran+hdf5+hdf5_compat~ipo+mpi~parmetis~python+shared~silo+test~zfp build_type=RelWithDebInfo patches=89d1829ad52f503f6179e43efddf998c239a95c14ca1f248463a3f61ad7d5cf7
    cub@1.12.0-rc0%gcc@9.3.0:
    cuda@11.4.2%gcc@9.3.0: ~dev
    cuda@11.4.2%gcc@11.1.0: ~dev
    curl@7.79.0%gcc@9.3.0: ~gssapi~ldap~libidn2~librtmp~libssh~libssh2~nghttp2 tls=openssl
    czmq@4.1.1%gcc@9.3.0:
    darshan-runtime@3.3.1%gcc@9.3.0: ~apmpi~apmpi_sync~apxc~hdf5+mpi scheduler=NONE
    darshan-util@3.3.1%gcc@9.3.0: ~apmpi~apxc~bzip2
    datatransferkit@3.1-rc3%gcc@9.3.0: ~external-arborx~ipo~openmp+serial+shared build_type=RelWithDebInfo
    diffutils@3.8%gcc@9.3.0:
    diffutils@3.8%gcc@11.1.0:
    double-conversion@3.1.5%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    doxygen@1.8.20%gcc@9.3.0: ~graphviz~ipo~mscgen build_type=RelWithDebInfo
    dtcmp@1.1.3%gcc@9.3.0: +shared
    dyninst@11.0.1%gcc@9.3.0: ~ipo+openmp~stat_dysect~static build_type=RelWithDebInfo
    eigen@3.4.0%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    elfutils@0.185%gcc@9.3.0: +bzip2~debuginfod~nls+xz patches=d786d49c28d7f0c8fc27bab39ca8714e5f4d128c7f09bb18533a8ec99b38dbf8
    er@0.1.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    exaworks@0.1.0%gcc@9.3.0:
    exmcutils@0.6.0%gcc@9.3.0:
    expat@2.4.1%gcc@9.3.0: +libbsd
    expat@2.4.1%gcc@11.1.0: +libbsd
    faodel@1.2108.1%gcc@9.3.0: ~hdf5~ipo+mpi+shared+tcmalloc build_type=RelWithDebInfo logging=stdout network=nnti serializer=xdr
    fftw@3.3.10%gcc@9.3.0: +mpi+openmp~pfft_patches precision=double,float
    findutils@4.8.0%gcc@9.3.0:
    flecsi@1.4.2%gcc@9.3.0: ~coverage~cuda~debug_backend~disable_metis~doc~doxygen+external_cinch~flecstan~flog~graphviz+hdf5~ipo~kokkos~openmp+shared~tutorial~unit_tests backend=mpi build_type=RelWithDebInfo caliper_detail=none cuda_arch=none
    flecsi@2.1.0%gcc@9.3.0: ~coverage+cuda~debug_backend~disable_metis~doc~doxygen~external_cinch~flecstan~flog~graphviz+hdf5~ipo~kokkos~openmp+shared~tutorial~unit_tests backend=mpi build_type=RelWithDebInfo caliper_detail=none cuda_arch=70
    flecsi@2.1.0%gcc@9.3.0: ~coverage+cuda~debug_backend~disable_metis~doc~doxygen~external_cinch~flecstan~flog~graphviz+hdf5~ipo~kokkos~openmp+shared~tutorial~unit_tests backend=mpi build_type=RelWithDebInfo caliper_detail=none cuda_arch=80
    flex@2.6.3%gcc@9.3.0: +lex~nls
    flit@2.1.0%gcc@9.3.0:
    flux-core@0.30.0%gcc@9.3.0: ~cuda~docs patches=0329f4b38b5ce4e1a3dee212cb93ce40388d1bde47c82a7a8e73bc7c38274c81
    flux-sched@0.19.0%gcc@9.3.0: ~cuda
    fortrilinos@2.0.0%gcc@9.3.0: +hl~ipo+shared build_type=RelWithDebInfo
    freetype@2.11.0%gcc@9.3.0:
    gasnet@2021.9.0%gcc@9.3.0: ~debug conduits=smp
    gawk@5.1.0%gcc@9.3.0:
    gdbm@1.19%gcc@9.3.0:
    gdbm@1.19%gcc@11.1.0:
    gettext@0.21%gcc@9.3.0: +bzip2+curses+git~libunistring+libxml2+tar+xz
    gettext@0.21%gcc@11.1.0: +bzip2+curses+git~libunistring+libxml2+tar+xz
    ginkgo@1.4.0%gcc@9.3.0: ~cuda~develtools~full_optimizations~hwloc~ipo+openmp+rocm+shared amdgpu_target=gfx908 build_type=Release cuda_arch=none patches=43e571a49ea774793d89d145571c95ae7cdf991f5220d8fe84db217a3c613a32
    ginkgo@1.4.0%gcc@9.3.0: ~cuda~develtools~full_optimizations~hwloc~ipo+openmp~rocm+shared amdgpu_target=none build_type=Release cuda_arch=none patches=43e571a49ea774793d89d145571c95ae7cdf991f5220d8fe84db217a3c613a32
    ginkgo@1.4.0%gcc@9.3.0: +cuda~develtools~full_optimizations~hwloc~ipo+openmp~rocm+shared amdgpu_target=none build_type=Release cuda_arch=70 patches=43e571a49ea774793d89d145571c95ae7cdf991f5220d8fe84db217a3c613a32
    ginkgo@1.4.0%gcc@9.3.0: +cuda~develtools~full_optimizations~hwloc~ipo+openmp~rocm+shared amdgpu_target=none build_type=Release cuda_arch=80 patches=43e571a49ea774793d89d145571c95ae7cdf991f5220d8fe84db217a3c613a32
    git@2.31.1%gcc@9.3.0: +man+nls+perl~svn~tcltk
    glib@2.70.0%gcc@9.3.0: ~libmount patches=8547954e50d0a720fa9794b84c31569df6a7419319b95ac9de0ed861d8969ffd tracing=none
    globalarrays@5.8%gcc@9.3.0: ~scalapack armci=mpi-ts
    glproto@1.4.17%gcc@9.3.0:
    gmake@4.3%gcc@9.3.0: ~guile+nls
    gmp@6.2.1%gcc@9.3.0:
    googletest@1.8.1%gcc@9.3.0: +gmock~ipo+pthreads+shared build_type=RelWithDebInfo
    gotcha@1.0.3%gcc@9.3.0: ~ipo~test build_type=RelWithDebInfo
    gperftools@2.9.1%gcc@9.3.0: +debugalloc~dynamic_sized_delete_support+libunwind~sized_delete
    gsl@2.7%gcc@9.3.0: ~external-cblas
    hdf5@1.8.22%gcc@9.3.0: ~cxx~fortran+hl~ipo~java+mpi+shared~szip~threadsafe+tools api=v18 build_type=RelWithDebInfo
    hdf5@1.8.22%gcc@9.3.0: ~cxx+fortran+hl~ipo~java+mpi+shared~szip~threadsafe+tools api=v18 build_type=RelWithDebInfo
    hdf5@1.12.0%gcc@9.3.0: ~cxx+fortran+hl~ipo~java+mpi+shared~szip~threadsafe+tools api=default build_type=RelWithDebInfo
    hdf5@1.12.1%gcc@9.3.0: ~cxx+fortran+hl~ipo~java+mpi+shared~szip~threadsafe+tools api=v18 build_type=RelWithDebInfo
    heffte@2.2.0%gcc@9.3.0: ~cuda~fftw~fortran~ipo~magma~mkl~python+rocm+shared amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none
    heffte@2.2.0%gcc@9.3.0: ~cuda+fftw~fortran~ipo~magma~mkl~python~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    heffte@2.2.0%gcc@9.3.0: +cuda~fftw~fortran~ipo~magma~mkl~python~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    heffte@2.2.0%gcc@9.3.0: +cuda~fftw~fortran~ipo~magma~mkl~python~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    help2man@1.47.16%gcc@9.3.0:
    hip@4.3.1%gcc@9.3.0: ~ipo build_type=Release patches=2a4190477b7d9206b9cd8d70770ba0bc007273cbe54772efb12f9ca2e37c0392,99190b4616edb362d48f9b265c3018a3c6339481b0729d9fe46185fca25bc54b,e276c4acf3d37712b6bea306fea34f539d3c4f743471e9da208b5eb17b16ae67
    hipblas@4.3.1%gcc@9.3.0: ~ipo build_type=Release
    hipsparse@4.3.1%gcc@9.3.0: ~ipo build_type=Release
    hpctoolkit@2021.10.15%gcc@9.3.0: ~all-static~cray~cuda~debug+mpi+papi~rocm+viewer
    hpcviewer@2021.10%gcc@9.3.0:
    hpx@1.7.1%gcc@9.3.0: ~async_cuda~async_mpi~cuda~examples~generic_coroutines~ipo~rocm~tools amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none cxxstd=17 instrumentation=none malloc=tcmalloc max_cpu_count=64 networking=tcp
    hpx@1.7.1%gcc@9.3.0: ~async_cuda~async_mpi+cuda~examples~generic_coroutines~ipo~rocm~tools amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70 cxxstd=17 instrumentation=none malloc=tcmalloc max_cpu_count=64 networking=tcp
    hpx@1.7.1%gcc@9.3.0: ~async_cuda~async_mpi+cuda~examples~generic_coroutines~ipo~rocm~tools amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80 cxxstd=17 instrumentation=none malloc=tcmalloc max_cpu_count=64 networking=tcp
    hsa-rocr-dev@4.3.1%gcc@9.3.0: +image~ipo+shared build_type=Release patches=71e6851d9be8113bfb8d71b66a3171e0d0401bae5e6f161c9e7fe32558261a46
    hwloc@2.6.0%gcc@9.3.0: ~cairo~cuda~gl~libudev+libxml2~netloc~nvml~opencl+pci~rocm+shared
    hwloc@2.6.0%gcc@9.3.0: ~cairo+cuda~gl~libudev+libxml2~netloc~nvml~opencl+pci~rocm+shared
    hwloc@2.6.0%gcc@11.1.0: ~cairo+cuda~gl~libudev+libxml2~netloc~nvml~opencl+pci~rocm+shared
    hypre@2.23.0%gcc@9.3.0: ~complex~cuda~debug+fortran~int64~internal-superlu~mixedint+mpi~openmp+shared~superlu-dist~unified-memory cuda_arch=none
    hypre@2.23.0%gcc@9.3.0: ~complex+cuda~debug+fortran~int64~internal-superlu~mixedint+mpi~openmp+shared~superlu-dist~unified-memory cuda_arch=70
    hypre@2.23.0%gcc@9.3.0: ~complex+cuda~debug+fortran~int64~internal-superlu~mixedint+mpi~openmp+shared~superlu-dist~unified-memory cuda_arch=80
    inputproto@2.3.2%gcc@9.3.0:
    intel-tbb@2020.3%gcc@9.3.0: ~ipo+shared+tm build_type=RelWithDebInfo cxxstd=default patches=62ba015ebd1819c45bef47411540b789b493e31ca668c4ff4cb2afcbc306b476,ce1fb16fb932ce86a82ca87cf0431d1a8c83652af9f552b264213b2ff2945d73,d62cb666de4010998c339cde6f41c7623a07e9fc69e498f2e149821c0c2c6dd0
    intel-xed@12.0.1%gcc@9.3.0: ~debug~pic patches=acffa07ee4b8e898df18f1439547e45ec9c609328dfb6e1ad2dce416587334d4
    jansson@2.13.1%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    json-c@0.15%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    jsoncpp@1.9.4%gcc@9.3.0: ~ipo build_type=RelWithDebInfo cxxstd=default
    kbproto@1.0.7%gcc@9.3.0:
    kokkos@3.4.01%gcc@9.3.0: ~aggressive_vectorization~compiler_warnings~cuda~cuda_constexpr~cuda_lambda~cuda_ldg_intrinsic~cuda_relocatable_device_code~cuda_uvm~debug~debug_bounds_check~debug_dualview_modify_check~deprecated_code~examples~explicit_instantiation~hpx~hpx_async_dispatch~hwloc~ipo~memkind~numactl~openmp~pic+profiling~profiling_load_print~pthread~qthread+rocm+serial+shared~sycl~tests~tuning~wrapper amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none std=14
    kokkos@3.4.01%gcc@9.3.0: ~aggressive_vectorization~compiler_warnings~cuda~cuda_constexpr~cuda_lambda~cuda_ldg_intrinsic~cuda_relocatable_device_code~cuda_uvm~debug~debug_bounds_check~debug_dualview_modify_check~deprecated_code~examples~explicit_instantiation~hpx~hpx_async_dispatch~hwloc~ipo~memkind~numactl~openmp~pic+profiling~profiling_load_print~pthread~qthread~rocm+serial+shared~sycl~tests~tuning~wrapper amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none std=14
    kokkos@3.4.01%gcc@9.3.0: ~aggressive_vectorization~compiler_warnings~cuda~cuda_constexpr~cuda_lambda~cuda_ldg_intrinsic~cuda_relocatable_device_code~cuda_uvm~debug~debug_bounds_check~debug_dualview_modify_check~deprecated_code~examples~explicit_instantiation~hpx~hpx_async_dispatch~hwloc~ipo~memkind~numactl+openmp~pic+profiling~profiling_load_print~pthread~qthread~rocm+serial+shared~sycl~tests~tuning~wrapper amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none std=14
    kokkos@3.4.01%gcc@9.3.0: ~aggressive_vectorization~compiler_warnings+cuda~cuda_constexpr~cuda_lambda~cuda_ldg_intrinsic~cuda_relocatable_device_code~cuda_uvm~debug~debug_bounds_check~debug_dualview_modify_check~deprecated_code~examples~explicit_instantiation~hpx~hpx_async_dispatch~hwloc~ipo~memkind~numactl~openmp~pic+profiling~profiling_load_print~pthread~qthread~rocm+serial+shared~sycl~tests~tuning+wrapper amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70 std=14
    kokkos@3.4.01%gcc@9.3.0: ~aggressive_vectorization~compiler_warnings+cuda~cuda_constexpr~cuda_lambda~cuda_ldg_intrinsic~cuda_relocatable_device_code~cuda_uvm~debug~debug_bounds_check~debug_dualview_modify_check~deprecated_code~examples~explicit_instantiation~hpx~hpx_async_dispatch~hwloc~ipo~memkind~numactl~openmp~pic+profiling~profiling_load_print~pthread~qthread~rocm+serial+shared~sycl~tests~tuning+wrapper amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80 std=14
    kokkos-kernels@3.4.01%gcc@9.3.0: ~blas~cblas~cublas~cuda~cusparse~ipo~lapack~lapacke~mkl+openmp~pthread~serial+shared~superlu build_type=RelWithDebInfo cuda_arch=none execspace_cuda=auto execspace_openmp=auto execspace_serial=auto execspace_threads=auto layouts=left memspace_cudaspace=auto memspace_cudauvmspace=auto offsets=int,size_t ordinals=int scalars=double
    kokkos-kernels@3.4.01%gcc@9.3.0: ~blas~cblas~cublas+cuda~cusparse~ipo~lapack~lapacke~mkl~openmp~pthread~serial+shared~superlu build_type=RelWithDebInfo cuda_arch=70 execspace_cuda=auto execspace_openmp=auto execspace_serial=auto execspace_threads=auto layouts=left memspace_cudaspace=auto memspace_cudauvmspace=auto offsets=int,size_t ordinals=int scalars=double
    kokkos-kernels@3.4.01%gcc@9.3.0: ~blas~cblas~cublas+cuda~cusparse~ipo~lapack~lapacke~mkl~openmp~pthread~serial+shared~superlu build_type=RelWithDebInfo cuda_arch=80 execspace_cuda=auto execspace_openmp=auto execspace_serial=auto execspace_threads=auto layouts=left memspace_cudaspace=auto memspace_cudauvmspace=auto offsets=int,size_t ordinals=int scalars=double
    kokkos-nvcc-wrapper@3.2.00%gcc@9.3.0: +mpi
    kokkos-nvcc-wrapper@3.2.00%gcc@9.3.0: +mpi
    kvtree@1.2.0%gcc@9.3.0: ~ipo+mpi+shared build_type=RelWithDebInfo file_lock=FLOCK
    lapackpp@2021.04.00%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    lapackpp@2021.04.00%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    lapackpp@2021.04.00%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    legion@21.03.0%gcc@9.3.0: ~bindings~bounds_checks~cuda~cuda_hijack~cuda_unsupported_compiler~enable_tls~fortran~gasnet_debug~hdf5~hwloc~ipo~kokkos+libdl~native~openmp~papi~privilege_checks~python~redop_complex~shared~spy+zlib build_type=RelWithDebInfo conduit=none cuda_arch=70 gasnet_root=none max_dims=3 max_fields=512 network=none output_level=warning
    libarchive@3.5.2%gcc@9.3.0: +iconv compression=bz2lib,lz4,lzma,lzo2,zlib,zstd crypto=mbedtls libs=shared,static programs=none xar=libxml2
    libbsd@0.11.3%gcc@9.3.0:
    libbsd@0.11.3%gcc@11.1.0:
    libcap@2.25%gcc@9.3.0:  patches=3db844dd771b320b8117b72f4e00b560051311fbbd4cba1ebcbd7c19116c1d66
    libcircle@0.3.0%gcc@9.3.0:
    libdwarf@20180129%gcc@9.3.0:
    libedit@3.1-20210216%gcc@9.3.0:
    libedit@3.1-20210216%gcc@11.1.0:
    libevent@2.1.12%gcc@9.3.0: +openssl
    libfabric@1.13.2%gcc@9.3.0: ~debug~kdreg fabrics=rxm,sockets,tcp,udp
    libffi@3.3%gcc@9.3.0:  patches=26f26c6f29a7ce9bf370ad3ab2610f99365b4bdd7b82e7c31df41a3370d685c0
    libffi@3.3%gcc@11.1.0:  patches=26f26c6f29a7ce9bf370ad3ab2610f99365b4bdd7b82e7c31df41a3370d685c0
    libgcrypt@1.9.3%gcc@9.3.0:
    libgit2@1.1.1%gcc@9.3.0: ~curl~ipo+ssh build_type=RelWithDebInfo https=system
    libgpg-error@1.42%gcc@9.3.0:
    libiberty@2.33.1%gcc@9.3.0: +pic
    libice@1.0.9%gcc@9.3.0:
    libiconv@1.16%gcc@9.3.0:  libs=shared,static
    libiconv@1.16%gcc@11.1.0:  libs=shared,static
    libidn2@2.3.0%gcc@9.3.0:
    libjpeg-turbo@2.1.0%gcc@9.3.0:
    libmd@1.0.3%gcc@9.3.0:
    libmd@1.0.3%gcc@11.1.0:
    libmonitor@2021.04.27%gcc@9.3.0: ~commrank~dlopen+hpctoolkit
    libnrm@0.1.0%gcc@9.3.0:
    libogg@1.3.4%gcc@9.3.0:
    libpciaccess@0.16%gcc@9.3.0:
    libpciaccess@0.16%gcc@11.1.0:
    libpng@1.6.37%gcc@9.3.0:
    libpthread-stubs@0.4%gcc@9.3.0:
    libquo@1.3.1%gcc@9.3.0:
    libsigsegv@2.13%gcc@9.3.0:
    libsigsegv@2.13%gcc@11.1.0:
    libsm@1.2.3%gcc@9.3.0:
    libsodium@1.0.18%gcc@9.3.0:
    libssh2@1.8.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    libtheora@1.1.1%gcc@9.3.0:  patches=057836ee5a5e28cae435c20d31173e20a3f96cfe680505836f19a77e985f7100,2e4f891f6880386d9391f3e4eaf4a23493de4eea532f9b5cb8a04b5f7cd09301,ff8478dbe3f19dd24f01d220e3262c1819d101b013d8ab3d7a972528252f1a7e
    libtiff@4.3.0%gcc@9.3.0: ~jbig~jpeg~jpeg12~lerc~libdeflate~lzma~old-jpeg~pixarlog~webp~zlib~zstd
    libtool@2.4.6%gcc@9.3.0:
    libtool@2.4.6%gcc@11.1.0:
    libunistring@0.9.10%gcc@9.3.0:
    libunwind@1.5.0%gcc@9.3.0: ~block_signals~conservative_checks~cxx_exceptions~debug~debug_frame+docs+pic+tests+weak_backtrace+xz~zlib components=none libs=shared,static
    libx11@1.7.0%gcc@9.3.0:
    libxau@1.0.8%gcc@9.3.0:
    libxcb@1.14%gcc@9.3.0:
    libxdmcp@1.1.2%gcc@9.3.0:
    libxext@1.3.3%gcc@9.3.0:
    libxml2@2.9.12%gcc@9.3.0: ~python
    libxml2@2.9.12%gcc@11.1.0: ~python
    libxt@1.1.5%gcc@9.3.0:
    libyaml@0.2.5%gcc@9.3.0:
    libyogrt@1.24%gcc@9.3.0: ~static scheduler=slurm
    libzmq@4.3.3%gcc@9.3.0: ~drafts+libsodium
    llvm-amdgpu@4.3.1%gcc@9.3.0: ~ipo+openmp+rocm-device-libs build_type=Release patches=d999f3b235e655ee07f6dd2590302082feaa06d32c5c6b53aae9c5cf1e45b644
    loki@0.1.7%gcc@9.3.0: +shared
    lua@5.3.5%gcc@9.3.0: ~pcfile+shared
    lua-luaposix@35.0%gcc@9.3.0:
    lwgrp@1.0.4%gcc@9.3.0: +shared
    lz4@1.9.3%gcc@9.3.0:  libs=shared,static
    lzo@2.10%gcc@9.3.0:  libs=shared,static
    m4@1.4.19%gcc@9.3.0: +sigsegv patches=9dc5fbd0d5cb1037ab1e6d0ecc74a30df218d0a94bdd5a02759a97f62daca573,bfdffa7c2eb01021d5849b36972c069693654ad826c1a20b53534009a4ec7a89
    m4@1.4.19%gcc@11.1.0: +sigsegv patches=9dc5fbd0d5cb1037ab1e6d0ecc74a30df218d0a94bdd5a02759a97f62daca573,bfdffa7c2eb01021d5849b36972c069693654ad826c1a20b53534009a4ec7a89
    magma@2.6.1%gcc@9.3.0: ~cuda+fortran~ipo+rocm+shared amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none
    magma@2.6.1%gcc@9.3.0: +cuda+fortran~ipo~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    magma@2.6.1%gcc@9.3.0: +cuda+fortran~ipo~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    mbedtls@2.27.0%gcc@9.3.0: +pic build_type=Release libs=static
    mbedtls@3.0.0%gcc@9.3.0: +pic build_type=Release libs=static
    memkind@1.10.1%gcc@9.3.0:
    mercury@1.0.1%gcc@9.3.0: +bmi+boostsys~cci+checksum~debug~ipo+mpi+ofi+shared+sm~udreg build_type=RelWithDebInfo patches=34fc95b3599c74a8cece6e873cfdc8bc0afe2dc0deabb6e2d11ea2a93f0cebf5
    mercury@2.0.1%gcc@9.3.0: ~bmi+boostsys~cci+checksum~debug~ipo+mpi+ofi+shared+sm~udreg build_type=RelWithDebInfo
    mesa18@18.3.6%gcc@9.3.0: +glx~llvm~opengles+osmesa patches=55a5611ca9fcbe8324c4d68a07b338134954ff12c5b122dc78ff376f012a1414 swr=none
    meson@0.60.0%gcc@9.3.0:  patches=aa6c50d5a2aeb1a487d16f6712be4357fefb923aae37ab830699b07338388287
    metall@0.17%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    metis@5.1.0%gcc@9.3.0: ~gdb~int64~real64+shared build_type=Release patches=4991da938c1d3a1d3dea78e49bbebecba00273f98df2a656e38b83d55b281da1,b1225da886605ea558db7ac08dd8054742ea5afe5ed61ad4d0fe7a495b1270d2
    mfem@4.3.0%gcc@9.3.0: ~amgx~conduit~cuda~debug~examples~gnutls~gslib~lapack~libceed~libunwind+metis~miniapps~mpfr+mpi~netcdf~occa~openmp~petsc~pumi~raja+rocm~shared~slepc+static~strumpack~suite-sparse~sundials~superlu-dist~threadsafe~umpire+zlib amdgpu_target=gfx908 cuda_arch=none patches=ccc98d6d874a760218c9f6e4bcf3a97ea79746b0f28997f4312fcf02a0beae6e timer=auto
    mfem@4.3.0%gcc@9.3.0: ~amgx~conduit~cuda~debug~examples~gnutls~gslib~lapack~libceed~libunwind+metis~miniapps~mpfr+mpi~netcdf~occa~openmp~petsc~pumi~raja~rocm~shared~slepc+static~strumpack~suite-sparse~sundials~superlu-dist~threadsafe~umpire+zlib amdgpu_target=none cuda_arch=none patches=ccc98d6d874a760218c9f6e4bcf3a97ea79746b0f28997f4312fcf02a0beae6e timer=auto
    mfem@4.3.0%gcc@9.3.0: ~amgx~conduit+cuda~debug~examples~gnutls~gslib~lapack~libceed~libunwind+metis~miniapps~mpfr+mpi~netcdf~occa~openmp~petsc~pumi~raja~rocm~shared~slepc+static~strumpack~suite-sparse~sundials~superlu-dist~threadsafe~umpire+zlib amdgpu_target=none cuda_arch=70 patches=ccc98d6d874a760218c9f6e4bcf3a97ea79746b0f28997f4312fcf02a0beae6e timer=auto
    mfem@4.3.0%gcc@9.3.0: ~amgx~conduit+cuda~debug~examples~gnutls~gslib~lapack~libceed~libunwind+metis~miniapps~mpfr+mpi~netcdf~occa~openmp~petsc~pumi~raja~rocm~shared~slepc+static~strumpack~suite-sparse~sundials~superlu-dist~threadsafe~umpire+zlib amdgpu_target=none cuda_arch=80 patches=ccc98d6d874a760218c9f6e4bcf3a97ea79746b0f28997f4312fcf02a0beae6e timer=auto
    mochi-margo@0.4.3%gcc@9.3.0:
    mpark-variant@1.4.0%gcc@9.3.0: ~ipo build_type=RelWithDebInfo patches=21a4f8de3525204ee6db2e53758a3e3fd9c13817df29d2926d24376858a369e7,4e173fe8c853eb92956a40371688b4a19498189fe65b7ceac30f6b9d6663a985,b3501f726fd40129b4aaa11453a5891c8953a34af8ac84f5ab10a22afa5e7b9b
    mpc@1.1.0%gcc@9.3.0:
    mpfr@4.1.0%gcc@9.3.0:
    mpich@3.4.2%gcc@9.3.0: ~argobots+fortran+hwloc+hydra+libxml2+pci+romio~slurm~verbs~wrapperrpath device=ch4 netmod=ofi pmi=pmi
    mpich@3.4.2%gcc@9.3.0: ~argobots+fortran+hwloc+hydra+libxml2+pci+romio~slurm~verbs~wrapperrpath device=ch4 netmod=ofi pmi=pmi
    mpifileutils@0.11%gcc@9.3.0: ~experimental~gpfs~lustre~xattr
    munge@0.5.14%gcc@9.3.0:  localstatedir=PREFIX/var
    muparser@2.2.6.1%gcc@9.3.0:
    nasm@2.15.05%gcc@9.3.0:
    ncurses@6.2%gcc@9.3.0: ~symlinks+termlib abi=none
    ncurses@6.2%gcc@11.1.0: ~symlinks+termlib abi=none
    netcdf-c@4.8.1%gcc@9.3.0: ~dap~fsync~hdf4~jna+mpi~parallel-netcdf+pic+shared
    netlib-scalapack@2.1.0%gcc@9.3.0: ~ipo~pic+shared build_type=Release patches=1c9ce5fee1451a08c2de3cc87f446aeda0b818ebbce4ad0d980ddf2f2a0b2dc4,f2baedde688ffe4c20943c334f580eb298e04d6f35c86b90a1f4e8cb7ae344a2
    ninja@1.10.2%gcc@9.3.0:
    nlohmann-json@3.10.4%gcc@9.3.0: ~ipo~multiple_headers build_type=RelWithDebInfo
    nrm@0.1.0%gcc@9.3.0:
    numactl@2.0.14%gcc@9.3.0:  patches=4e1d78cbbb85de625bad28705e748856033eaafab92a66dffd383a3d7e00cc94,62fc8a8bf7665a60e8f4c93ebbd535647cebf74198f7afafec4c085a8825c006,ff37630df599cfabf0740518b91ec8daaf18e8f288b19adaae5364dc1f6b2296
    nvhpc@21.9%gcc@9.3.0: +blas+lapack+mpi install_type=single
    oce@0.18.3%gcc@9.3.0: ~X11+tbb
    omega-h@9.34.1%gcc@9.3.0: ~examples~ipo+mpi+optimize+shared+symbols~throw+trilinos~warnings+zlib build_type=RelWithDebInfo
    openblas@0.3.18%gcc@9.3.0: ~bignuma~consistent_fpcsr~ilp64+locking+pic+shared threads=openmp
    openjdk@11.0.12_7%gcc@9.3.0:
    openmpi@4.1.1%gcc@9.3.0: ~atomics~cuda~cxx~cxx_exceptions+gpfs~internal-hwloc~java~legacylaunchers~lustre~memchecker~pmi~pmix~singularity~sqlite3+static~thread_multiple+vt+wrapper-rpath fabrics=none schedulers=none
    openpmd-api@0.14.3%gcc@9.3.0: ~adios1+adios2+hdf5~ipo+mpi~python+shared build_type=RelWithDebInfo
    openssh@8.7p1%gcc@9.3.0:
    openssl@1.1.1l%gcc@9.3.0: ~docs certs=system
    openssl@1.1.1l%gcc@11.1.0: ~docs certs=system
    otf2@2.3%gcc@9.3.0:
    papi@6.0.0.1%gcc@9.3.0: ~cuda+example~infiniband~lmsensors~nvml~powercap~rapl~rocm~rocm_smi~sde+shared~static_tools amdgpu_target=none
    papyrus@1.0.1%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    parallel-netcdf@1.12.2%gcc@9.3.0: ~burstbuffer+cxx+fortran+pic+shared
    paraview@5.9.1%gcc@9.3.0: ~adios2~advanced_debug~cuda+development_files~examples~hdf5~ipo+kits+mpi+opengl2~osmesa~python~python3~qt+shared build_edition=canonical build_type=RelWithDebInfo cuda_arch=none
    parmetis@4.0.3%gcc@9.3.0: ~gdb~int64~ipo+shared build_type=RelWithDebInfo patches=4f892531eb0a807eb1b82e683a416d3e35154a455274cf9b162fb02054d11a5b,50ed2081bc939269689789942067c58b3e522c269269a430d5d34c00edbc5870,704b84f7c7444d4372cb59cca6e1209df4ef3b033bc4ee3cf50f369bce972a9d
    parmetis@4.0.3%gcc@9.3.0: ~gdb~int64~ipo+shared build_type=RelWithDebInfo patches=4f892531eb0a807eb1b82e683a416d3e35154a455274cf9b162fb02054d11a5b,50ed2081bc939269689789942067c58b3e522c269269a430d5d34c00edbc5870,704b84f7c7444d4372cb59cca6e1209df4ef3b033bc4ee3cf50f369bce972a9d
    parsec@3.0.2012%gcc@9.3.0: ~cuda~debug_verbose~ipo~profile+shared build_type=RelWithDebInfo cuda_arch=none
    parsec@3.0.2012%gcc@9.3.0: +cuda~debug_verbose~ipo~profile+shared build_type=RelWithDebInfo cuda_arch=70
    parsec@3.0.2012%gcc@9.3.0: +cuda~debug_verbose~ipo~profile+shared build_type=RelWithDebInfo cuda_arch=80
    pcre@8.44%gcc@9.3.0: ~jit+multibyte+utf
    pcre@8.44%gcc@11.1.0: ~jit+multibyte+utf
    pcre2@10.36%gcc@9.3.0: ~jit+multibyte
    pdsh@2.31%gcc@9.3.0: +ssh+static_modules
    pdt@3.25.1%gcc@9.3.0: ~pic
    pegtl@3.2.0%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    perl@5.34.0%gcc@9.3.0: +cpanm+shared+threads
    perl@5.34.0%gcc@11.1.0: +cpanm+shared+threads
    perl-data-dumper@2.173%gcc@11.1.0:
    petsc@3.16.1%gcc@9.3.0: ~X~batch~cgns~complex~cuda~debug+double~exodusii~fftw~giflib+hdf5~hpddm~hwloc+hypre~int64~jpeg~knl~libpng~libyaml~memkind+metis~mkl-pardiso~mmg~moab~mpfr+mpi~mumps~openmp~p4est~parmmg~ptscotch~random123~rocm~saws~scalapack+shared~strumpack~suite-sparse+superlu-dist~tetgen~trilinos~valgrind amdgpu_target=none clanguage=C cuda_arch=none
    petsc@3.16.1%gcc@9.3.0: ~X~batch~cgns~complex+cuda~debug+double~exodusii~fftw~giflib+hdf5~hpddm~hwloc+hypre~int64~jpeg~knl~libpng~libyaml~memkind+metis~mkl-pardiso~mmg~moab~mpfr+mpi~mumps~openmp~p4est~parmmg~ptscotch~random123~rocm~saws~scalapack+shared~strumpack~suite-sparse+superlu-dist~tetgen~trilinos~valgrind amdgpu_target=none clanguage=C cuda_arch=70
    petsc@3.16.1%gcc@9.3.0: ~X~batch~cgns~complex+cuda~debug+double~exodusii~fftw~giflib+hdf5~hpddm~hwloc+hypre~int64~jpeg~knl~libpng~libyaml~memkind+metis~mkl-pardiso~mmg~moab~mpfr+mpi~mumps~openmp~p4est~parmmg~ptscotch~random123~rocm~saws~scalapack+shared~strumpack~suite-sparse+superlu-dist~tetgen~trilinos~valgrind amdgpu_target=none clanguage=C cuda_arch=80
    petsc@3.16.1%gcc@9.3.0: ~X~batch~cgns~complex+cuda~debug+double~exodusii~fftw~giflib+hdf5~hpddm~hwloc+hypre~int64~jpeg~knl~libpng~libyaml~memkind+metis~mkl-pardiso~mmg~moab~mpfr+mpi~mumps~openmp~p4est~parmmg~ptscotch~random123~rocm~saws~scalapack+shared~strumpack~suite-sparse+superlu-dist~tetgen~trilinos~valgrind amdgpu_target=none clanguage=C cuda_arch=none
    pflotran@3.0.2%gcc@9.3.0:
    pfunit@3.3.3%gcc@9.3.0: ~docs~ipo+mpi~openmp+shared~use_comm_world build_type=RelWithDebInfo max_array_rank=5
    phist@1.9.5%gcc@9.3.0: +fortran+host+int64~ipo+mpi+openmp~parmetis+scamac+shared~trilinos build_type=RelWithDebInfo kernel_lib=builtin outlev=2 patches=1fc9ac6c1cce2804d171dd12470e709cd1e3839be6d51f7e5c33427ba4b379ca
    pkgconf@1.8.0%gcc@9.3.0:
    pkgconf@1.8.0%gcc@11.1.0:
    plasma@21.8.29%gcc@9.3.0: ~ipo~lua+shared build_type=RelWithDebInfo
    precice@2.3.0%gcc@9.3.0: ~ipo+mpi+petsc~python+shared build_type=RelWithDebInfo
    protobuf@3.18.0%gcc@9.3.0: +shared build_type=Release
    pugixml@1.11.4%gcc@9.3.0: ~ipo+pic+shared build_type=RelWithDebInfo
    pumi@2.2.6%gcc@9.3.0: ~fortran~int64~ipo~shared+simmodsuite_version_check~testing~zoltan build_type=RelWithDebInfo simmodsuite=none
    py-alabaster@0.7.12%gcc@9.3.0:
    py-alembic@1.5.5%gcc@9.3.0:
    py-anyio@3.3.4%gcc@9.3.0:
    py-apache-libcloud@1.2.1%gcc@9.3.0:
    py-argon2-cffi@21.1.0%gcc@9.3.0:
    py-async-generator@1.10%gcc@9.3.0:
    py-attrs@21.2.0%gcc@9.3.0:
    py-babel@2.7.0%gcc@9.3.0:
    py-backcall@0.2.0%gcc@9.3.0:
    py-bcrypt@3.2.0%gcc@9.3.0:
    py-beniget@0.4.1%gcc@9.3.0:
    py-bleach@4.1.0%gcc@9.3.0:
    py-blinker@1.4%gcc@9.3.0:
    py-bottleneck@1.3.2%gcc@9.3.0:
    py-certifi@2021.10.8%gcc@9.3.0:
    py-certipy@0.1.3%gcc@9.3.0:
    py-cffi@1.15.0%gcc@9.3.0:
    py-chardet@4.0.0%gcc@9.3.0:
    py-cinemasci@1.3%gcc@9.3.0: +mpi
    py-colorama@0.4.4%gcc@9.3.0:
    py-cryptography@3.4.7%gcc@9.3.0: ~idna
    py-cython@0.29.24%gcc@9.3.0:
    py-cython@0.29.24%gcc@9.3.0:
    py-debugpy@1.4.1%gcc@9.3.0:
    py-decorator@5.1.0%gcc@9.3.0:
    py-defusedxml@0.7.1%gcc@9.3.0:
    py-dill@0.3.4%gcc@9.3.0:
    py-docutils@0.17.1%gcc@9.3.0:
    py-entrypoints@0.3%gcc@9.3.0:
    py-fuzzywuzzy@0.18.0%gcc@9.3.0: ~speedup
    py-gast@0.5.2%gcc@9.3.0:
    py-gevent@1.5.0%gcc@9.3.0:
    py-globus-sdk@3.0.2%gcc@9.3.0:
    py-greenlet@1.1.0%gcc@9.3.0:
    py-h5py@3.4.0%gcc@9.3.0: +mpi
    py-idna@2.9%gcc@9.3.0:
    py-imagesize@1.1.0%gcc@9.3.0:
    py-iniconfig@1.1.1%gcc@9.3.0:
    py-ipykernel@6.4.1%gcc@9.3.0:
    py-ipython@7.28.0%gcc@9.3.0:
    py-ipython-genutils@0.2.0%gcc@9.3.0:
    py-ipywidgets@7.6.5%gcc@9.3.0:
    py-jedi@0.18.0%gcc@9.3.0:
    py-jinja2@3.0.1%gcc@9.3.0: ~i18n
    py-json5@0.9.4%gcc@9.3.0:
    py-jsonpatch@1.23%gcc@9.3.0:
    py-jsonpointer@1.9%gcc@9.3.0:
    py-jsonschema@2.6.0%gcc@9.3.0:
    py-jsonschema@3.2.0%gcc@9.3.0:
    py-jupyter-client@6.1.12%gcc@9.3.0:
    py-jupyter-core@4.7.1%gcc@9.3.0:
    py-jupyter-packaging@0.7.12%gcc@9.3.0:
    py-jupyter-server@1.9.0%gcc@9.3.0:
    py-jupyter-telemetry@0.1.0%gcc@9.3.0:
    py-jupyterhub@1.4.1%gcc@9.3.0:
    py-jupyterlab@3.0.14%gcc@9.3.0:
    py-jupyterlab-pygments@0.1.2%gcc@9.3.0:
    py-jupyterlab-server@2.6.0%gcc@9.3.0:
    py-jupyterlab-widgets@1.0.2%gcc@9.3.0:
    py-libensemble@0.8.0%gcc@9.3.0: ~deap+mpi~mpmath~nlopt~petsc4py~pyyaml~scipy~tasmanian
    py-mako@1.1.4%gcc@9.3.0:
    py-markupsafe@2.0.1%gcc@9.3.0:
    py-matplotlib-inline@0.1.2%gcc@9.3.0:
    py-mistune@0.8.4%gcc@9.3.0:
    py-mpi4py@3.1.2%gcc@9.3.0:
    py-msgpack@1.0.2%gcc@9.3.0:
    py-mypy@0.910%gcc@9.3.0: ~python2
    py-mypy-extensions@0.4.3%gcc@9.3.0:
    py-nbclassic@0.3.1%gcc@9.3.0:
    py-nbclient@0.5.0%gcc@9.3.0:
    py-nbconvert@6.0.1%gcc@9.3.0: +serve
    py-nbformat@5.1.3%gcc@9.3.0:
    py-nest-asyncio@1.4.0%gcc@9.3.0:
    py-netifaces@0.10.5%gcc@9.3.0:
    py-notebook@6.1.4%gcc@9.3.0:
    py-ntplib@0.4.0%gcc@9.3.0:
    py-numexpr@2.7.3%gcc@9.3.0:
    py-numpy@1.21.3%gcc@9.3.0: +blas+lapack patches=873745d7b547857fcfec9cae90b09c133b42a4f0c23b6c2d84cf37e2dd816604
    py-numpy@1.21.3%gcc@9.3.0: +blas+lapack patches=873745d7b547857fcfec9cae90b09c133b42a4f0c23b6c2d84cf37e2dd816604
    py-oauthlib@3.1.0%gcc@9.3.0: +extras
    py-packaging@21.0%gcc@9.3.0:
    py-pamela@1.0.0%gcc@9.3.0:
    py-pandas@1.3.4%gcc@9.3.0:
    py-pandocfilters@1.4.2%gcc@9.3.0:
    py-paramiko@2.7.1%gcc@9.3.0: ~invoke
    py-parse@1.18.0%gcc@9.3.0:
    py-parsl@1.1.0%gcc@9.3.0:
    py-parso@0.8.1%gcc@9.3.0:
    py-pbr@5.4.3%gcc@9.3.0:
    py-periodictable@1.5.0%gcc@9.3.0:
    py-petsc4py@3.16.1%gcc@9.3.0: +mpi patches=d344e0e8b4c5ebb96961e1905bdd749162c52b151538238609f4dec77e2a6360
    py-pexpect@4.7.0%gcc@9.3.0:
    py-pickleshare@0.7.5%gcc@9.3.0:
    py-picmistandard@0.0.14%gcc@9.3.0:
    py-pika@0.13.0%gcc@9.3.0:
    py-pillow@8.0.0%gcc@9.3.0: ~freetype~imagequant+jpeg~jpeg2000~lcms~tiff~webp~webpmux~xcb+zlib
    py-pip@21.1.2%gcc@9.3.0:
    py-pip@21.1.2%gcc@9.3.0:
    py-pkgconfig@1.5.1%gcc@9.3.0:
    py-pluggy@0.13.0%gcc@9.3.0:
    py-ply@3.11%gcc@9.3.0:
    py-prometheus-client@0.7.1%gcc@9.3.0: ~twisted
    py-prompt-toolkit@3.0.17%gcc@9.3.0:
    py-psutil@5.8.0%gcc@9.3.0:
    py-ptyprocess@0.7.0%gcc@9.3.0:
    py-py@1.9.0%gcc@9.3.0:
    py-pybind11@2.6.2%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    py-pycparser@2.20%gcc@9.3.0:
    py-pyelftools@0.26%gcc@9.3.0:
    py-pygments@2.10.0%gcc@9.3.0:
    py-pyjwt@2.1.0%gcc@9.3.0: ~crypto
    py-pyjwt@2.1.0%gcc@9.3.0: +crypto
    py-pymongo@3.9.0%gcc@9.3.0:
    py-pynacl@1.4.0%gcc@9.3.0:
    py-pyopenssl@19.0.0%gcc@9.3.0:
    py-pyparsing@2.4.7%gcc@9.3.0:
    py-pyrsistent@0.15.7%gcc@9.3.0:
    py-pytest@6.2.4%gcc@9.3.0:
    py-pytest-runner@5.1%gcc@9.3.0:
    py-python-dateutil@2.8.2%gcc@9.3.0:
    py-python-editor@1.0.4%gcc@9.3.0:
    py-python-json-logger@0.1.11%gcc@9.3.0:
    py-pythran@0.9.12%gcc@9.3.0:  patches=b427dcaaa74ed4dfa39c58819e446e86055b65648826a101c08634d3c0be1f95
    py-pytz@2021.1%gcc@9.3.0:
    py-pyyaml@5.3.1%gcc@9.3.0: +libyaml
    py-pyzmq@17.1.2%gcc@9.3.0:
    py-pyzmq@18.1.0%gcc@9.3.0:
    py-radical-entk@1.8.0%gcc@9.3.0:
    py-radical-gtod@1.6.7%gcc@9.3.0:
    py-radical-pilot@1.8.0%gcc@9.3.0:
    py-radical-saga@1.8.0%gcc@9.3.0:
    py-radical-utils@1.8.0%gcc@9.3.0:
    py-regex@2020.11.13%gcc@9.3.0:
    py-requests@2.25.1%gcc@9.3.0: ~socks
    py-requests-unixsocket@0.2.0%gcc@9.3.0:
    py-ruamel-yaml@0.17.16%gcc@9.3.0:
    py-ruamel-yaml-clib@0.2.0%gcc@9.3.0:
    py-scipy@1.7.1%gcc@9.3.0:
    py-semantic-version@2.8.2%gcc@9.3.0:
    py-send2trash@1.5.0%gcc@9.3.0:
    py-setproctitle@1.1.10%gcc@9.3.0:
    py-setuptools@57.4.0%gcc@9.3.0:
    py-setuptools@58.2.0%gcc@9.3.0:
    py-setuptools-rust@0.12.1%gcc@9.3.0:
    py-setuptools-scm@6.3.2%gcc@9.3.0: +toml
    py-setuptools-scm@6.3.2%gcc@9.3.0: +toml
    py-six@1.16.0%gcc@9.3.0:
    py-sniffio@1.2.0%gcc@9.3.0:
    py-snowballstemmer@2.0.0%gcc@9.3.0:
    py-sphinx@4.1.2%gcc@9.3.0:
    py-sphinx-autodoc-typehints@1.12.0%gcc@9.3.0:
    py-sphinx-rtd-theme@0.5.1%gcc@9.3.0:
    py-sphinxcontrib-applehelp@1.0.1%gcc@9.3.0:
    py-sphinxcontrib-devhelp@1.0.1%gcc@9.3.0:
    py-sphinxcontrib-htmlhelp@2.0.0%gcc@9.3.0:
    py-sphinxcontrib-jsmath@1.0.1%gcc@9.3.0:
    py-sphinxcontrib-qthelp@1.0.2%gcc@9.3.0:
    py-sphinxcontrib-serializinghtml@1.1.5%gcc@9.3.0:
    py-sphobjinv@2.1%gcc@9.3.0:
    py-sqlalchemy@1.4.20%gcc@9.3.0:  backend=none
    py-tblib@1.6.0%gcc@9.3.0:
    py-terminado@0.8.3%gcc@9.3.0:
    py-testpath@0.4.2%gcc@9.3.0:
    py-toml@0.10.2%gcc@9.3.0:
    py-tomli@1.2.1%gcc@9.3.0:
    py-tornado@5.1.1%gcc@9.3.0:
    py-tornado@6.1%gcc@9.3.0:
    py-traitlets@5.0.4%gcc@9.3.0:
    py-typed-ast@1.4.3%gcc@9.3.0:
    py-typeguard@2.12.1%gcc@9.3.0:
    py-typing-extensions@3.10.0.2%gcc@9.3.0:
    py-urllib3@1.26.6%gcc@9.3.0: ~brotli~secure~socks
    py-vcversioner@2.16.0.0%gcc@9.3.0:
    py-warlock@1.3.3%gcc@9.3.0:
    py-warpx@21.11%gcc@9.3.0: +mpi
    py-warpx@21.11%gcc@9.3.0: +mpi
    py-warpx@21.11%gcc@9.3.0: +mpi
    py-wcwidth@0.1.7%gcc@9.3.0:
    py-webencodings@0.5.1%gcc@9.3.0:
    py-websocket-client@0.57.0%gcc@9.3.0:
    py-wheel@0.36.2%gcc@9.3.0:
    py-widgetsnbextension@3.5.1%gcc@9.3.0:
    python@3.8.12%gcc@9.3.0: +bz2+ctypes+dbm~debug+libxml2+lzma~nis~optimizations+pic+pyexpat+pythoncmd+readline+shared+sqlite3+ssl~tix~tkinter~ucs4+uuid+zlib patches=0d98e93189bc278fbc37a50ed7f183bd8aaf249a8e1670a465f0db6bb4f8cf87,4c2457325f2b608b1b6a2c63087df8c26e07db3e3d493caf36a56f0ecf6fb768,f2fd060afc4b4618fe8104c4c5d771f36dc55b1db5a4623785a4ea707ec72fb4
    python@3.8.12%gcc@11.1.0: +bz2+ctypes+dbm~debug+libxml2+lzma~nis~optimizations+pic+pyexpat+pythoncmd+readline+shared+sqlite3+ssl~tix~tkinter~ucs4+uuid+zlib patches=0d98e93189bc278fbc37a50ed7f183bd8aaf249a8e1670a465f0db6bb4f8cf87,4c2457325f2b608b1b6a2c63087df8c26e07db3e3d493caf36a56f0ecf6fb768,f2fd060afc4b4618fe8104c4c5d771f36dc55b1db5a4623785a4ea707ec72fb4
    qthreads@1.16%gcc@9.3.0: +hwloc~spawn_cache+static scheduler=distrib stack_size=4096
    raja@0.13.0%gcc@9.3.0: ~cuda+examples+exercises~ipo+openmp~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none patches=57dd531a50ac791b4bb214d34a4bf3fca1349354927c72915b7ccd20524701a9
    raja@0.14.0%gcc@9.3.0: ~cuda+examples+exercises~ipo+openmp~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    raja@0.14.0%gcc@9.3.0: +cuda+examples+exercises~ipo+openmp~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    raja@0.14.0%gcc@9.3.0: +cuda+examples+exercises~ipo+openmp~rocm+shared~tests amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    rankstr@0.1.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    readline@8.1%gcc@9.3.0:
    readline@8.1%gcc@11.1.0:
    redset@0.1.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    rempi@1.1.0%gcc@9.3.0:
    rocblas@4.3.1%gcc@9.3.0: ~ipo build_type=Release tensile_architecture=all
    rocfft@4.3.1%gcc@9.3.0: ~ipo amdgpu_target=gfx701 amdgpu_target_sram_ecc=none build_type=Release
    rocrand@4.3.1%gcc@9.3.0: ~ipo build_type=Release
    rocsolver@4.3.1%gcc@9.3.0: ~ipo+optimal amdgpu_target=gfx906:xnack- build_type=Release
    rust@1.51.0%gcc@9.3.0: +analysis+clippy~rls+rustfmt+src extra_targets=none
    scr@3.0rc2%gcc@9.3.0: +bbapi~bbapi_fallback~dw+examples+fortran~ipo+libyogrt+pdsh+shared+tests build_type=RelWithDebInfo cache_base=/dev/shm cntl_base=/dev/shm copy_config=none file_lock=FLOCK resource_manager=SLURM scr_config=scr.conf
    shuffile@0.1.0%gcc@9.3.0: ~ipo+shared build_type=RelWithDebInfo
    slate@2021.05.02%gcc@9.3.0: ~cuda~ipo+mpi+openmp+rocm+shared amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none
    slate@2021.05.02%gcc@9.3.0: ~cuda~ipo+mpi+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    slate@2021.05.02%gcc@9.3.0: +cuda~ipo+mpi+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    slate@2021.05.02%gcc@9.3.0: +cuda~ipo+mpi+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    slepc@3.16.0%gcc@9.3.0: +arpack~blopex~cuda~rocm amdgpu_target=none cuda_arch=none
    slepc@3.16.0%gcc@9.3.0: +arpack~blopex+cuda~rocm amdgpu_target=none cuda_arch=70
    slepc@3.16.0%gcc@9.3.0: +arpack~blopex+cuda~rocm amdgpu_target=none cuda_arch=80
    slurm@21-08-1-1%gcc@9.3.0: ~gtk~hdf5~hwloc~mariadb~pmix+readline~restd sysconfdir=PREFIX/etc
    snappy@1.1.8%gcc@9.3.0: ~ipo+pic+shared build_type=RelWithDebInfo patches=c9cfecb1f7a623418590cf4e00ae7d308d1c3faeb15046c2e5090e38221da7cd
    spath@0.1.0%gcc@9.3.0: ~ipo~mpi+shared build_type=RelWithDebInfo
    spath@0.1.0%gcc@9.3.0: ~ipo+mpi+shared build_type=RelWithDebInfo
    sqlite@3.36.0%gcc@9.3.0: +column_metadata+fts~functions~rtree
    sqlite@3.36.0%gcc@11.1.0: +column_metadata+fts~functions~rtree
    stc@0.9.0%gcc@9.3.0:
    strumpack@6.1.0%gcc@9.3.0: +butterflypack+c_interface~count_flops~cuda~ipo+mpi+openmp+parmetis+rocm~scotch+shared~slate~task_timers+zfp amdgpu_target=gfx908 build_type=RelWithDebInfo cuda_arch=none
    strumpack@6.1.0%gcc@9.3.0: +butterflypack+c_interface~count_flops~cuda~ipo+mpi+openmp+parmetis~rocm~scotch+shared~slate~task_timers+zfp amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none
    strumpack@6.1.0%gcc@9.3.0: +butterflypack+c_interface~count_flops+cuda~ipo+mpi+openmp+parmetis~rocm~scotch+shared~slate~task_timers+zfp amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70
    strumpack@6.1.0%gcc@9.3.0: +butterflypack+c_interface~count_flops+cuda~ipo+mpi+openmp+parmetis~rocm~scotch+shared~slate~task_timers+zfp amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80
    suite-sparse@5.10.1%gcc@9.3.0: ~cuda~openmp+pic~tbb
    sundials@5.8.0%gcc@9.3.0: +ARKODE+CVODE+CVODES+IDA+IDAS+KINSOL~cuda+examples+examples-install~f2003~fcmix+generic-math~hypre~int64~ipo~klu~lapack~monitoring+mpi~openmp~petsc~pthread~raja~rocm+shared+static~superlu-dist~superlu-mt~sycl~trilinos amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none precision=double
    sundials@5.8.0%gcc@9.3.0: +ARKODE+CVODE+CVODES+IDA+IDAS+KINSOL+cuda+examples+examples-install~f2003~fcmix+generic-math~hypre~int64~ipo~klu~lapack~monitoring+mpi~openmp~petsc~pthread~raja~rocm+shared+static~superlu-dist~superlu-mt~sycl~trilinos amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70 precision=double
    sundials@5.8.0%gcc@9.3.0: +ARKODE+CVODE+CVODES+IDA+IDAS+KINSOL+cuda+examples+examples-install~f2003~fcmix+generic-math~hypre~int64~ipo~klu~lapack~monitoring+mpi~openmp~petsc~pthread~raja~rocm+shared+static~superlu-dist~superlu-mt~sycl~trilinos amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80 precision=double
    superlu@5.3.0%gcc@9.3.0: ~ipo+pic build_type=RelWithDebInfo
    superlu-dist@7.1.1%gcc@9.3.0: ~cuda~int64~ipo~openmp+shared build_type=RelWithDebInfo cuda_arch=none
    superlu-dist@7.1.1%gcc@9.3.0: ~cuda~int64~ipo~openmp+shared build_type=RelWithDebInfo cuda_arch=none
    superlu-dist@7.1.1%gcc@9.3.0: +cuda~int64~ipo~openmp+shared build_type=RelWithDebInfo cuda_arch=70
    superlu-dist@7.1.1%gcc@9.3.0: +cuda~int64~ipo~openmp+shared build_type=RelWithDebInfo cuda_arch=80
    swig@4.0.2%gcc@9.3.0:
    swig@4.0.2%gcc@11.1.0:
    swig@4.0.2-fortran%gcc@9.3.0:
    symengine@0.7.0%gcc@9.3.0: ~boostmp~flint~ipo~llvm+mpc+mpfr~openmp~piranha+shared+thread_safe build_type=Release
    sz@1.4.12.3%gcc@9.3.0: ~fortran~hdf5~ipo~netcdf~pastri~python~random_access+shared~stats~time_compression build_type=RelWithDebInfo
    sz@2.1.12%gcc@9.3.0: ~fortran~hdf5~ipo~netcdf~pastri~python~random_access+shared~stats~time_compression build_type=RelWithDebInfo
    tar@1.34%gcc@9.3.0:
    tar@1.34%gcc@11.1.0:
    tasmanian@7.7%gcc@9.3.0: ~blas~cuda~fortran~ipo~magma+mpi+openmp~python~rocm~xsdkflags amdgpu_target=none build_type=Release cuda_arch=none
    tasmanian@7.7%gcc@9.3.0: ~blas+cuda~fortran~ipo~magma+mpi+openmp~python~rocm~xsdkflags amdgpu_target=none build_type=Release cuda_arch=70
    tasmanian@7.7%gcc@9.3.0: ~blas+cuda~fortran~ipo~magma+mpi+openmp~python~rocm~xsdkflags amdgpu_target=none build_type=Release cuda_arch=80
    tau@2.31%gcc@9.3.0: ~adios2+binutils~comm~craycnl~cuda+elf+fortran~gasnet+io~level_zero+libdwarf+libunwind~likwid+mpi~ompt~opari~opencl~openmp+otf2+papi+pdt~phase~ppc64le~profileparam+pthreads+python~rocm~rocprofiler~roctracer~scorep~shmem~sqlite~x86_64
    tcl@8.6.11%gcc@9.3.0:
    texinfo@6.5%gcc@9.3.0:  patches=12f6edb0c6b270b8c8dba2ce17998c580db01182d871ee32b7b6e4129bd1d23a,1732115f651cff98989cb0215d8f64da5e0f7911ebf0c13b064920f088f2ffe1
    trilinos@13.0.1%gcc@9.3.0: ~adios2+amesos+amesos2+anasazi+aztec~basker+belos+boost~chaco~complex~cuda~cuda_rdc~debug~dtk+epetra+epetraext~epetraextbtf~epetraextexperimental~epetraextgraphreorderings~exodus+explicit_template_instantiation~float+fortran~gtest~hdf5~hypre+ifpack+ifpack2+intrepid+intrepid2~ipo+isorropia+kokkos~mesquite+minitensor+ml+mpi+muelu~mumps+nox~openmp+phalanx+piro~python+rol+rythmos+sacado~scorec+shards+shared+shylu+stk+stokhos+stratimikos~strumpack~suite-sparse~superlu+superlu-dist+teko+tempus+tpetra+trilinoscouplings~wrapper~x11+zoltan+zoltan2 build_type=RelWithDebInfo cuda_arch=none cxxstd=14 gotype=long_long
    trilinos@13.2.0%gcc@9.3.0: ~adios2+amesos+amesos2+anasazi+aztec~basker+belos+boost~chaco~complex+cuda~cuda_rdc~debug~dtk+epetra+epetraext~epetraextbtf~epetraextexperimental~epetraextgraphreorderings~exodus+explicit_template_instantiation~float+fortran~gtest~hdf5~hypre+ifpack+ifpack2+intrepid+intrepid2~ipo+isorropia+kokkos~mesquite+minitensor+ml+mpi+muelu~mumps+nox~openmp+phalanx+piro~python+rol+rythmos+sacado~scorec+shards+shared+shylu+stk+stokhos+stratimikos~strumpack~suite-sparse~superlu+superlu-dist+teko+tempus+tpetra+trilinoscouplings+wrapper~x11+zoltan+zoltan2 build_type=RelWithDebInfo cuda_arch=70 cxxstd=14 gotype=long_long
    trilinos@13.2.0%gcc@9.3.0: ~adios2+amesos+amesos2+anasazi+aztec~basker+belos+boost~chaco~complex+cuda~cuda_rdc~debug~dtk+epetra+epetraext~epetraextbtf~epetraextexperimental~epetraextgraphreorderings~exodus+explicit_template_instantiation~float+fortran~gtest~hdf5~hypre+ifpack+ifpack2+intrepid+intrepid2~ipo+isorropia+kokkos~mesquite+minitensor+ml+mpi+muelu~mumps+nox~openmp+phalanx+piro~python+rol+rythmos+sacado~scorec+shards+shared+shylu+stk+stokhos+stratimikos~strumpack~suite-sparse~superlu+superlu-dist+teko+tempus+tpetra+trilinoscouplings+wrapper~x11+zoltan+zoltan2 build_type=RelWithDebInfo cuda_arch=80 cxxstd=14 gotype=long_long
    turbine@1.3.0%gcc@9.3.0: ~hdf5~python~r
    umap@2.1.0%gcc@9.3.0: ~ipo~logging~tests build_type=RelWithDebInfo
    umpire@5.0.1%gcc@9.3.0: +c~cuda~deviceconst+examples~fortran~ipo~numa+openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none patches=135bbc7d2f371531f432672b115ac0a407968aabfffc5b8a941db9b493dbf81f,f5c691752e4833a936bce224bbe0fe884d3afa84c5e5a4a481f59a12840159c9 tests=none
    umpire@6.0.0%gcc@9.3.0: +c~cuda~deviceconst+examples~fortran~ipo~numa~openmp~rocm+shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=none tests=none
    umpire@6.0.0%gcc@9.3.0: +c+cuda~deviceconst+examples~fortran~ipo~numa~openmp~rocm~shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=70 tests=none
    umpire@6.0.0%gcc@9.3.0: +c+cuda~deviceconst+examples~fortran~ipo~numa~openmp~rocm~shared amdgpu_target=none build_type=RelWithDebInfo cuda_arch=80 tests=none
    unifyfs@0.9.1%gcc@9.3.0: +auto-mount~fortran~pmi~pmix+spath patches=8a9c20c857c728637d994c097505cdce780f4b8e61535d221117864f75795313,f6d61ac7cee0d690f50a9a99ff9d2242f494c3baf7374baaf6779340728566b1
    unzip@6.0%gcc@9.3.0:
    upcxx@2021.9.0%gcc@9.3.0: ~cuda~gasnet+mpi cross=none
    upcxx@2021.9.0%gcc@9.3.0: +cuda~gasnet+mpi cross=none
    util-linux-uuid@2.36.2%gcc@9.3.0:
    util-linux-uuid@2.36.2%gcc@11.1.0:
    util-macros@1.19.3%gcc@9.3.0:
    util-macros@1.19.3%gcc@11.1.0:
    veloc@1.5%gcc@9.3.0: ~ipo build_type=RelWithDebInfo
    vtk-h@0.7.1%gcc@9.3.0: ~contourtree~cuda~logging+mpi+openmp+serial+shared cuda_arch=none
    vtk-m@1.6.0%gcc@9.3.0: ~64bitids+ascent_types~cuda+doubleprecision~hip~ipo~logging+mpi+openmp+rendering~shared~tbb~virtuals amdgpu_target=none build_type=Release cuda_arch=none
    vtk-m@1.6.0%gcc@9.3.0: ~64bitids+ascent_types+cuda+doubleprecision~hip~ipo~logging+mpi+openmp+rendering~shared~tbb~virtuals amdgpu_target=none build_type=Release cuda_arch=70
    vtk-m@1.6.0%gcc@9.3.0: ~64bitids+ascent_types+cuda+doubleprecision~hip~ipo~logging+mpi+openmp+rendering~shared~tbb~virtuals amdgpu_target=none build_type=Release cuda_arch=80
    warpx@21.11%gcc@9.3.0: +app~ascent~eb~ipo+lib+mpi+mpithreadmultiple+openpmd+psatd+qed~qedtablegen+shared+tprof build_type=RelWithDebInfo compute=omp dims=2 precision=double
    warpx@21.11%gcc@9.3.0: +app~ascent~eb~ipo+lib+mpi+mpithreadmultiple+openpmd+psatd+qed~qedtablegen+shared+tprof build_type=RelWithDebInfo compute=omp dims=3 precision=double
    warpx@21.11%gcc@9.3.0: +app~ascent~eb~ipo+lib+mpi+mpithreadmultiple+openpmd+psatd+qed~qedtablegen+shared+tprof build_type=RelWithDebInfo compute=omp dims=rz precision=double
    xcb-proto@1.14.1%gcc@9.3.0:
    xerces-c@3.2.3%gcc@9.3.0:  cxxstd=default netaccessor=curl transcoder=iconv
    xextproto@7.3.0%gcc@9.3.0:
    xproto@7.0.31%gcc@9.3.0:
    xtrans@1.3.5%gcc@9.3.0:
    xz@5.2.5%gcc@9.3.0: +pic libs=shared,static
    xz@5.2.5%gcc@11.1.0: +pic libs=shared,static
    yaml-cpp@0.7.0%gcc@9.3.0: ~ipo+pic+shared~tests build_type=RelWithDebInfo
    z3@4.8.9%gcc@11.1.0: ~gmp~ipo~python build_type=RelWithDebInfo
    zfp@0.5.5%gcc@9.3.0: ~aligned~c~cuda~fasthash~fortran~ipo~openmp~profile~python+shared~strided~twoway bsws=64 build_type=RelWithDebInfo cuda_arch=none
    zfp@0.5.5%gcc@9.3.0: ~aligned~c+cuda~fasthash~fortran~ipo~openmp~profile~python+shared~strided~twoway bsws=64 build_type=RelWithDebInfo cuda_arch=70
    zfp@0.5.5%gcc@9.3.0: ~aligned~c+cuda~fasthash~fortran~ipo~openmp~profile~python+shared~strided~twoway bsws=64 build_type=RelWithDebInfo cuda_arch=80
    zlib@1.2.11%gcc@9.3.0: +optimize+pic+shared
    zlib@1.2.11%gcc@11.1.0: +optimize+pic+shared
    zsh@5.8%gcc@9.3.0: +skip-tcsetpgrp-test
    zstd@1.5.0%gcc@9.3.0: ~programs
    ```

## Need Help with Spack

If you need help with Spack, please join the [Spack Slack Channel](https://slack.spack.io/), once you have registered you
can go to slack workspace at https://spackpm.slack.com/.
