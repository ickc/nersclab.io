# PI Toolbox 

The [PI Toolbox](https://my.nersc.gov/pitools/) allows PIs and their proxies to make
changes to files owned by project members on the Community File System. You can

- Change the group for files and directories
- Change the owner of files and directories
- Change the group-level [unix permissions](unix-file-permissions.md) on files and directories
- Make an entire project directory group readable in one action

## Choosing Files and Directories

To make changes, start by selecting a project directory for which you are the PI or PI
proxy from the **Jump to** menu at the top. This sets the tool to make changes in that
particular directory tree. If you are not a PI or PI proxy, you will see no project
directories listed, and you will not be able to use the tool.

One you've selected a project directory, you should see the files and directories it
contains in the file list area below. You can navigate further through the project
directory tree by clicking directories in the file list or by typing a path in the **Path**
box. The file list is pulled from the live file system, and you can update the list to
reflect any changes since you last loaded the list by clicking **Refresh file list**. 

## Actions

### Making an Entire Project Directory Group Readable

After selecting a project directory, you can click **Make entire project dir group
readable**, which will make two changes. It will change the group of all files and
directories within the project directory to the project directory's assigned group, and it
will make them all group readable. If you click the button, a dialog box will let you know
which group it will use and will ask you to confirm.

To make changes to specific files and directories, click the checkboxes to toggle them on
for the ones you want to change. You can use the **Select All** checkbox to select or deselect
every item in the current directory. When you navigate to a different directory level,
your selections will be deselected, to prevent accidental changes. Once you've toggled
your selections on, you can click one of the actions below. The actions also allow you to
make changes recursively. It's generally best to use the recursive option rather than to
select each item in a directory separately, as the toolbox issues fewer commands and
completes sooner that way.

### Changing the Group of Specific Files and Directories

Click **Change Group** to set the group of selected files and directories to any group of
which you are a member. You'll be presented with a dialog box to select the group and to
choose whether you want the change to be recursive.

### Changing the Owner of Specific Files and Directories

Click **Change Owner** to set the owner of selected files and directories to any user in any
of the projects you manage. This option also allows you to set the unix group for the
selected files and directories. You'll be presented with a dialog box to select the
username and group name and to choose whether you want the change to be recursive.

### Changing Permissions on Specific Files and Directories

Click **Change Permissions** to set new group-level unix permissions on selected files and
directories. The permissions you select will replace existing permissions for the group,
so for example, if you select only **Read** for a file that currently has group read and write
permissions, the result will be a file with only group read permissions. You'll be
presented with a dialog box to select the permissions you want to enable and to choose
whether you want the change to be recursive.

## Pending and Completed Requests

When you submit a request with the toolbox, your changes will be queued for completion by
a process that initiates requested changes every five minutes. Depending on how extensive
the requested changes are, the process may complete right away or it may take up to
several hours. Most requests complete right away. 

You can review the state of your requests by checking the **Pending Requests** list and the
**Requests Completed in the Last Month**. Click the title of one of these areas to reveal or
hide the list. These lists show the specific commands the system initiated (or will
initiate) on your behalf. They also show the time the request was submitted and the
status. For completed requests, you can click the status text to see the output generated
by running the command. You can use the **Refresh** button in either list to update it,
showing how your requests have progressed since you last opened the list or refreshed it.
